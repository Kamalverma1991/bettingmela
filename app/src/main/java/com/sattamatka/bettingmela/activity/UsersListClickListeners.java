package com.sattamatka.bettingmela.activity;

public interface UsersListClickListeners {
    public void onUsersIdClickForTransactions(int position);
    public void onUsersCreditDebitAdd(int position);
    public void onUsersEdit(int position);
}
