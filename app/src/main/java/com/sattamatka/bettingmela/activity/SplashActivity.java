package com.sattamatka.bettingmela.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.MainActivity;
import com.sattamatka.bettingmela.MyApplication;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.model.UserData;
import com.sattamatka.bettingmela.model.response.BaseResponse;
import com.sattamatka.bettingmela.utils.Encryption;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_CHECKUSEREXISTS;

public class SplashActivity extends AppCompatActivity {

    private MyPrefs myPrefs;
    //    private TextView tvPesakmao;
    private Encryption encryption;
    //    private VolleyWrappers volleyWrappers;
    private Handler handler;
    final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 1;


    ProgressBar progressBar;
    private VolleyWrappers volleyWrappers = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        initView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissionCheck();
        } else {


            if (myPrefs.isLogincontractor()) {
                progressBar.setVisibility(View.VISIBLE);
            }

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!myPrefs.isLogincontractor()) {
                        startActivity(new Intent(SplashActivity.this, ContractorRegistration.class));
                        finish();
                    } else {
                        requestForCheckUserExist(myPrefs.getUsername());
                    }

                }
            }, 2000);


        }
    }

    private void initView() {
        handler = new Handler();
        myPrefs = new MyPrefs(SplashActivity.this);
        volleyWrappers = new VolleyWrappers(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void permissionCheck() {
        int readphoneState = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
        int contacts = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int sms = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> permissions = new ArrayList<String>();
        if (readphoneState != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (contacts != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (sms != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!permissions.isEmpty()) {
            requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
        } else {

            if (myPrefs.isLogincontractor()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!myPrefs.isLogincontractor()) {
                        startActivity(new Intent(SplashActivity.this, ContractorRegistration.class));
                        finish();
                    } else {
                        requestForCheckUserExist(myPrefs.getUsername());

                    }

                }
            }, 2000);

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_SOME_FEATURES_PERMISSIONS: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                            if (i == permissions.length - 1) {

                                if (myPrefs.isLogincontractor()) {
                                    progressBar.setVisibility(View.VISIBLE);
                                }
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!myPrefs.isLogincontractor()) {
                                            startActivity(new Intent(SplashActivity.this, ContractorRegistration.class));
                                            finish();
                                        } else {
                                            requestForCheckUserExist(myPrefs.getUsername());
                                        }

                                    }
                                }, 1500);
                            }
                        }

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private void requestForCheckUserExist(String username) {
        try {
            UserData userData = new UserData();
            userData.setUsername(username);
            Gson gson = new Gson();
            String jsonString = gson.toJson(userData);
            Logger.logDebug("kamal", " requestLogin jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestLogin url : " + MyConstants.URL_CONTRACTOR_CHECKUSEREXIST);
            JSONObject request = new JSONObject(jsonString);

            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_CHECKUSEREXIST, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progressBar.setVisibility(View.GONE);
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestLogin response : " + response.toString());
                                Gson gson = new Gson();
                                BaseResponse baseResponse = gson.fromJson(response.toString(), BaseResponse.class);
                                if (baseResponse.getError().equalsIgnoreCase("false")) {
                                    MyApplication.getInstance().setBaseResponse(baseResponse);
                                    myPrefs.setLogincontractor(true);
                                    myPrefs.setId(baseResponse.getUserData().getId());
                                    myPrefs.setUsername(baseResponse.getUserData().getUsername());
                                    myPrefs.setEmail(baseResponse.getUserData().getEmail());
                                    myPrefs.setMobile(baseResponse.getUserData().getMobile());
                                    myPrefs.setAmount(baseResponse.getUserData().getAmount());
                                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(SplashActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(SplashActivity.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SplashActivity.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestLogin error : " + error.getLocalizedMessage());
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SplashActivity.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                });

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_CHECKUSEREXISTS);
                progressBar.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(SplashActivity.this, MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }


}

