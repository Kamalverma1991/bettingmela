package com.sattamatka.bettingmela.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.MainActivity;
import com.sattamatka.bettingmela.MyApplication;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.model.UserData;
import com.sattamatka.bettingmela.model.response.BaseResponse;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;
import com.sattamatka.bettingmela.utils.UsefullUtils;

import org.json.JSONObject;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_CHECKUSEREXISTS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_FORGOTPASSWORD;
import static com.sattamatka.bettingmela.utils.Tags.TAG_LOGIN;
import static com.sattamatka.bettingmela.utils.Tags.TAG_RESETPASSWORD;

public class ContractorRegistration extends AppCompatActivity implements View.OnClickListener {

    TextView tvHeading;

    LinearLayout llLoginForm;
    EditText etLoginform_email;
    EditText etLoginform_password;
    TextView tvLoginform_Login;
    TextView tvLoginform_Fogotpassword;

    LinearLayout llForgetPasswordForm;
    EditText etForgotpasswordform_email;
    EditText etForgotpasswordform_password;
    EditText etForgotpasswordform_confirmpassword;
    EditText etForgotpasswordform_otp;
    TextView tvForgotpasswordform_Submit;
    TextView tvForgotpasswordform_login;


    Dialog dialogLoading;
    private MyPrefs myPrefs;
    private VolleyWrappers volleyWrappers = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contractor_registration);
        myPrefs = new MyPrefs(this);
        volleyWrappers = new VolleyWrappers(this);
//        //signin
//        restSignIn = ServiceGenerator.createService(RestInterface.class, RestInterface.FLIPANIMATION_NC);
//        tilMobile = (TextInputLayout) findViewById(R.id.phoneTilP);
//        tilPassword = (TextInputLayout) findViewById(R.id.passwordTil);
//        etMobile = (EditText) findViewById(R.id.edittext_phone);
//        etPassword = (EditText) findViewById(R.id.edittext_password);
        tvHeading = findViewById(R.id.textview_heading);

        llLoginForm = findViewById(R.id.linearlayout_loginform);
        etLoginform_email = findViewById(R.id.loginform_edittext_email);
        etLoginform_password = findViewById(R.id.loginform_edittext_password);
        tvLoginform_Login = (TextView) findViewById(R.id.textviewbutton_login);
        tvLoginform_Fogotpassword = (TextView) findViewById(R.id.text_forgotpassword);
        tvLoginform_Login.setOnClickListener(this);
        tvLoginform_Fogotpassword.setOnClickListener(this);

        llForgetPasswordForm = findViewById(R.id.linearlayout_forgotpassword);
        etForgotpasswordform_email = findViewById(R.id.forgotpassword_edittext_email);
        etForgotpasswordform_password = findViewById(R.id.forgotpassword_edittext_password);
        etForgotpasswordform_confirmpassword = findViewById(R.id.forgotpassword_edittext_confirmpassword);
        etForgotpasswordform_otp = findViewById(R.id.forgotpassword_edittext_otp);
        tvForgotpasswordform_Submit = findViewById(R.id.forgotpassword_textviewbutton_submitemail);
        tvForgotpasswordform_login = findViewById(R.id.text_login);
        tvForgotpasswordform_Submit.setOnClickListener(this);
        tvForgotpasswordform_login.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == tvLoginform_Login) {
            logIn();

        }
        if (view == tvLoginform_Fogotpassword) {
            Toast.makeText(ContractorRegistration.this, "Click Forgot paa", Toast.LENGTH_SHORT).show();
            tvHeading.setText("Contractor Forgot Password");
            llLoginForm.setVisibility(View.GONE);
            llForgetPasswordForm.setVisibility(View.VISIBLE);
        }
        if (view == tvForgotpasswordform_Submit) {
            if (etForgotpasswordform_password.getVisibility() == View.GONE && etForgotpasswordform_confirmpassword.getVisibility() == View.GONE && etForgotpasswordform_otp.getVisibility() == View.GONE) {
                forgotpassword();
            } else {
               resetpassword();
            }

        }
        if (view == tvForgotpasswordform_login) {
            tvHeading.setText("Contractor Login");
            llLoginForm.setVisibility(View.VISIBLE);
            llForgetPasswordForm.setVisibility(View.GONE);

            etForgotpasswordform_password.setVisibility(View.GONE);
            etForgotpasswordform_confirmpassword.setVisibility(View.GONE);
            etForgotpasswordform_otp.setVisibility(View.GONE);
        }

    }

    private void logIn() {
        if (TextUtils.isEmpty(etLoginform_email.getText().toString())) {
            etLoginform_email.setError("Please enter username");
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(etLoginform_password.getText())) {
            etLoginform_password.setError("Please enter password");
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
        } else {
            requestLogin(etLoginform_email.getText().toString().trim(), etLoginform_password.getText().toString().trim());
        }
    }

    private void forgotpassword() {
        if (TextUtils.isEmpty(etForgotpasswordform_email.getText().toString())) {
            etForgotpasswordform_email.setError("Please enter username");
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
        } else {
            requestForForgotpassword(etForgotpasswordform_email.getText().toString().trim());
        }
    }
    private void resetpassword() {

        if (TextUtils.isEmpty(etForgotpasswordform_email.getText().toString())) {
            etForgotpasswordform_email.setError("Please enter username");
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(etForgotpasswordform_password.getText())) {
            etForgotpasswordform_password.setError("Please enter password");
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(etForgotpasswordform_confirmpassword.getText())) {
            etForgotpasswordform_confirmpassword.setError("please confirm your password.");
            Toast.makeText(this, "please confirm your password.", Toast.LENGTH_SHORT).show();
        } else if (!etForgotpasswordform_confirmpassword.getText().toString().equals(etForgotpasswordform_password.getText().toString())) {
            etForgotpasswordform_confirmpassword.setError("Password didn't match");
            Toast.makeText(this, "Password didn't match", Toast.LENGTH_LONG).show();
        }else  if (TextUtils.isEmpty(etForgotpasswordform_otp.getText().toString())) {
            etForgotpasswordform_otp.setError("Please enter otp");
            Toast.makeText(this, "Please enter otp", Toast.LENGTH_SHORT).show();
        }else{
            requestForResetpassword(etForgotpasswordform_email.getText().toString().trim(),etForgotpasswordform_confirmpassword.getText().toString().trim(),etForgotpasswordform_otp.getText().toString().trim());

        }
    }
    private void requestLogin(String email, String password) {
        try {
            UserData userData = new UserData();
            userData.setUsername(email);
            userData.setPassword(password);
            userData.setIp(UsefullUtils.getLocalIpAddress());
            userData.setBrowser("app");
            userData.setOs("android");
            Gson gson = new Gson();
            String jsonString = gson.toJson(userData);
            Logger.logDebug("kamal", " requestLogin jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestLogin url : " + MyConstants.URL_CONTRACTOR_LOGIN);
            JSONObject request = new JSONObject(jsonString);

            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_LOGIN, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("kamaltest 1 ");
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            System.out.println("kamaltest try 2 ");
                            if (response != null) {
                                Logger.logDebug("kamal", " requestLogin response : " + response.toString());
                                Gson gson = new Gson();
                                BaseResponse baseResponse = gson.fromJson(response.toString(), BaseResponse.class);
                                if (baseResponse.getError().equalsIgnoreCase("false")) {
                                    MyApplication.getInstance().setBaseResponse(baseResponse);
                                    myPrefs.setLogincontractor(true);
                                    myPrefs.setId(baseResponse.getUserData().getId());
                                    myPrefs.setUsername(baseResponse.getUserData().getUsername());
                                    myPrefs.setMobile(baseResponse.getUserData().getMobile());
                                    myPrefs.setAmount(baseResponse.getUserData().getAmount());
                                    startActivity(new Intent(ContractorRegistration.this, MainActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(ContractorRegistration.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                System.out.println("kamaltest response null 3 ");
                                Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("kamaltest catch 4 ");
                            Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestLogin error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        System.out.println("kamaltest onErrorResponse 5 ");
                        Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                });

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_LOGIN);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
        }

    }

    private void requestForForgotpassword(String username) {
        try {
            UserData userData = new UserData();
            userData.setUsername(username);
            Gson gson = new Gson();
            String jsonString = gson.toJson(userData);
            Logger.logDebug("kamal", " requestforgotpass jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestforgotpass url : " + MyConstants.URL_CONTRACTOR_FORGOT_PASSWORD);
            JSONObject request = new JSONObject(jsonString);

            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_FORGOT_PASSWORD, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestforgotpass response : " + response.toString());
                                Gson gson = new Gson();
                                BaseResponse baseResponse = gson.fromJson(response.toString(), BaseResponse.class);
                                if (baseResponse.getError().equalsIgnoreCase("false")) {
                                    Logger.logDebug("kamal", " requestforgotpass error : " + baseResponse.getError());
//                                    Please check your email\/mobile to generate new password.
                                        Toast.makeText(ContractorRegistration.this, baseResponse.getMessage(), Toast.LENGTH_LONG).show();
                                    etForgotpasswordform_password.setVisibility(View.VISIBLE);
                                    etForgotpasswordform_confirmpassword.setVisibility(View.VISIBLE);
                                    etForgotpasswordform_otp.setVisibility(View.VISIBLE);

                                } else {
                                    Toast.makeText(ContractorRegistration.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestLogin error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                });

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_FORGOTPASSWORD);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }
    private void requestForResetpassword(String username, String pass, String otp) {
        try {
            UserData userData = new UserData();
            userData.setUsername(username);
            userData.setPassword(pass);
            userData.setOtp(otp);
            Gson gson = new Gson();
            String jsonString = gson.toJson(userData);
            Logger.logDebug("kamal", " requestForResetpassword jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForResetpassword url : " + MyConstants.URL_CONTRACTOR_RESET_PASSWORD);
            JSONObject request = new JSONObject(jsonString);

            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_RESET_PASSWORD, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForResetpassword response : " + response.toString());
                                Gson gson = new Gson();
                                BaseResponse baseResponse = gson.fromJson(response.toString(), BaseResponse.class);
                                if (baseResponse.getError().equalsIgnoreCase("false")) {
                                    Logger.logDebug("kamal", " requestForResetpassword error : " + baseResponse.getError());

                                    Toast.makeText(ContractorRegistration.this, baseResponse.getMessage(), Toast.LENGTH_LONG).show();

                                    tvHeading.setText("Contractor Login");
                                    llLoginForm.setVisibility(View.VISIBLE);
                                    llForgetPasswordForm.setVisibility(View.GONE);

                                    etForgotpasswordform_password.setVisibility(View.GONE);
                                    etForgotpasswordform_confirmpassword.setVisibility(View.GONE);
                                    etForgotpasswordform_otp.setVisibility(View.GONE);

                                } else {
                                    Toast.makeText(ContractorRegistration.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForResetpassword error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                });

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_RESETPASSWORD);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(ContractorRegistration.this, MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }
    private void dialogOpeningApp() {
        dialogLoading = new Dialog(ContractorRegistration.this);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


    }
}


