package com.sattamatka.bettingmela.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.adapters.AdapterEditSheet;
import com.sattamatka.bettingmela.model.response.ResponseSheetAddedOrUpdated;
import com.sattamatka.bettingmela.model.response.ResponseSheetByIdInfoNoArray;
import com.sattamatka.bettingmela.model.response.ResponseSheetInfoDetail;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.UsefullUtils;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_ADDORUPDATESHEET;

public class ContractorAddSheet extends AppCompatActivity implements View.OnClickListener {


    String user_id = "";
    String sheet_id = "";
    String window_name = "";
    String window_id = "";
    String booking_date = "";
    Dialog dialogLoading;
    private VolleyWrappers volleyWrappers = null;
    //    private List<ResponseSheetByIdInfoNoArray> listDummy = new ArrayList<>();
    private List<ResponseSheetByIdInfoNoArray> listinfo = new ArrayList<>();
    private List<Integer> listPerNumTotal = new ArrayList<>();

    private ImageView ivBack;
    private RelativeLayout rlTimer;
    private TextView tvTimerRemaining;
    private TextView tvWindowName;
    private TextView tvBookingdate;
    private TextView tvLastBookingtime;
    private TextView tvTotal;

    private RecyclerView recyclerView;
    private AdapterEditSheet adapterEditSheet;
    List<ResponseSheetInfoDetail> listSheetInfo;

    String selectedCheckbox = "none";
    private LinearLayout llEditor;
    private CheckBox cbPair;
    private CheckBox cbSetsWithpair;
    private CheckBox cbSetsWithoutpair;
    private CheckBox cbSeries;

    private EditText etAb;
    private EditText etInputNumber;
    private EditText etSet1;
    private EditText etSet2;
    private EditText etAmount;
    private TextView tvGo;
    private TextView tvSubmit;

    TreeMap<Integer, String> treeMap_NumAmount = new TreeMap<>();
    String aaaa;
    int keyDel;
    CountDownTimer cTimer = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.contractor_addsheet);

//        listSheetInfo = ((List<ResponseSheetInfoDetail>) getIntent().getExtras().getSerializable("list"));
        initViews();
        checkboxWorks();
        recyclerviewWorks();


        tvWindowName.setText(window_name);
        tvBookingdate.setText(booking_date);
        if (window_id.equalsIgnoreCase("1")) {
            tvLastBookingtime.setText("16:00:00");
        } else if (window_id.equalsIgnoreCase("2")) {
            tvLastBookingtime.setText("17:40:00");
        } else if (window_id.equalsIgnoreCase("3")) {
            tvLastBookingtime.setText("19:40:00");
        } else if (window_id.equalsIgnoreCase("4")) {
            tvLastBookingtime.setText("22:30:00");
        } else if (window_id.equalsIgnoreCase("5")) {
            tvLastBookingtime.setText("02:30:00");
        }


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd, HH:mm:ss");
        String currentDateandTime = new SimpleDateFormat("yyyy.MM.dd, HH:mm:ss", Locale.getDefault()).format(new Date());
//        String windowDateTime = "2019.09.09, 16:00:00";//Timer date 2
        String windowDateTime = booking_date.replaceAll("-", ".") + ", " + tvLastBookingtime.getText().toString();//Timer date 2
        long diff = 0;
        long curentLong;
        long windowDateTimeLong;
        Date currentDate, newDate;
        try {
            currentDate = formatter.parse(currentDateandTime);
            newDate = formatter.parse(windowDateTime);
            curentLong = currentDate.getTime();
            windowDateTimeLong = newDate.getTime();
            diff = windowDateTimeLong - curentLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startTimer(diff);

    }


    private void initViews() {
        user_id = getIntent().getStringExtra("user_id");
        sheet_id = getIntent().getStringExtra("sheet_id");
        window_name = getIntent().getStringExtra("window_name");
        window_id = getIntent().getStringExtra("window_id");
        booking_date = getIntent().getStringExtra("booking_date");

        volleyWrappers = new VolleyWrappers(ContractorAddSheet.this);
        ivBack = findViewById(R.id.imageview_back);
        rlTimer = findViewById(R.id.linearlayout_timer);
        tvTimerRemaining = findViewById(R.id.textview_timerremaining);
        tvWindowName = findViewById(R.id.textview_windowname);
        tvBookingdate = findViewById(R.id.textview_bookingdate);
        tvLastBookingtime = findViewById(R.id.textview_lastbookingtime);
        tvTotal = findViewById(R.id.textview_total);
        recyclerView = findViewById(R.id.recycler_view);

//        radioGroup = (RadioGroup) findViewById(R.id.groupradio);
        llEditor = (LinearLayout) findViewById(R.id.linearlayout_editor);
        cbPair = (CheckBox) findViewById(R.id.checkbox_pair);
        cbSetsWithpair = (CheckBox) findViewById(R.id.checkbox_sets_withpair);
        cbSetsWithoutpair = (CheckBox) findViewById(R.id.checkbox_sets_withoutpair);
        cbSeries = (CheckBox) findViewById(R.id.checkbox_series);
//        radioGroup.clearCheck();


        etAb = findViewById(R.id.edittext_ab);
        etInputNumber = findViewById(R.id.edittext_inputnumbers);
        etSet1 = findViewById(R.id.edittext_set1);
        etSet2 = findViewById(R.id.edittext_set2);
        etAmount = findViewById(R.id.edittext_amount);
        tvGo = findViewById(R.id.textview_go);
        tvSubmit = findViewById(R.id.textview_submit);
        tvGo.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etAb.setEnabled(true);
        etAb.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focusVaule) {
                if (focusVaule) {
                    etInputNumber.setText("");
                }
            }
        });
        etInputNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etAb.getText().toString().equalsIgnoreCase("")) {
                    boolean flag = true;
                    String eachBlock[] = etInputNumber.getText().toString().split("-");
                    for (int i = 0; i < eachBlock.length; i++) {
                        if (eachBlock[i].length() > 2) {
                            flag = false;
                        }
                    }
                    if (flag) {

                        etInputNumber.setOnKeyListener(new View.OnKeyListener() {

                            @Override
                            public boolean onKey(View v, int keyCode, KeyEvent event) {

                                if (keyCode == KeyEvent.KEYCODE_DEL)
                                    keyDel = 1;
                                return false;
                            }
                        });

                        if (keyDel == 0) {

                            if (((etInputNumber.getText().length() + 1) % 3) == 0) {
                                etInputNumber.setText(etInputNumber.getText() + "-");
                                etInputNumber.setSelection(etInputNumber.getText().length());

                            }
                            aaaa = etInputNumber.getText().toString();
                        } else {
                            aaaa = etInputNumber.getText().toString();
                            keyDel = 0;
                        }

                    } else {
                        etInputNumber.setText(aaaa);
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    private void checkboxWorks() {
        cbPair.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cbPair.isChecked()) {
                    cbPair.setChecked(true);
                    cbSetsWithpair.setChecked(false);
                    cbSetsWithpair.setChecked(false);
                    cbSetsWithoutpair.setChecked(false);
                    cbSeries.setChecked(false);

                    selectedCheckbox = "pair";
                    etAb.setEnabled(false);
                    etInputNumber.setEnabled(false);
                    etAb.setVisibility(View.GONE);
                    etInputNumber.setText("11,22,33,44,55,66,77,88,99,100");
                    etAmount.setText("");
                    etSet1.setVisibility(View.GONE);
                    etSet2.setVisibility(View.GONE);
                } else {
//                    cbPair.setChecked(false);
                    selectedCheckbox = "none";
                    etAb.setEnabled(true);
                    etAb.setVisibility(View.VISIBLE);
                    etInputNumber.setEnabled(true);
                    etInputNumber.setText("");
                }
            }
        });
        cbSetsWithpair.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cbSetsWithpair.isChecked()) {
                    cbPair.setChecked(false);
                    cbSetsWithpair.setChecked(true);
                    cbSetsWithoutpair.setChecked(false);
                    cbSeries.setChecked(false);
//
                    selectedCheckbox = "setswithpair";
                    etAb.setEnabled(false);
                    etInputNumber.setText("");
                    etAb.setVisibility(View.GONE);
                    etInputNumber.setVisibility(View.GONE);
                    etSet1.setVisibility(View.VISIBLE);
                    etSet1.setHint("Set1");
                    etSet1.setText("");
                    etSet2.setVisibility(View.VISIBLE);
                    etSet2.setHint("Set2");
                    etSet2.setText("");
                    etAmount.setText("");
                } else {

                    selectedCheckbox = "none";
                    etAb.setEnabled(true);
                    etInputNumber.setEnabled(true);
                    etInputNumber.setText("");
                    etAb.setVisibility(View.VISIBLE);
                    etInputNumber.setVisibility(View.VISIBLE);
                }
            }
        });
        cbSetsWithoutpair.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cbSetsWithoutpair.isChecked()) {
                    cbPair.setChecked(false);
                    cbSetsWithpair.setChecked(false);
                    cbSetsWithoutpair.setChecked(true);
                    cbSeries.setChecked(false);
//
                    selectedCheckbox = "setswithoutpair";
                    etAb.setEnabled(false);
                    etInputNumber.setText("");
                    etAb.setVisibility(View.GONE);
                    etInputNumber.setVisibility(View.GONE);
                    etSet1.setVisibility(View.VISIBLE);
                    etSet1.setHint("Set1");
                    etSet1.setText("");
                    etSet2.setVisibility(View.VISIBLE);
                    etSet2.setHint("Set2");
                    etSet2.setText("");
                    etAmount.setText("");
                } else {

                    selectedCheckbox = "none";
                    etAb.setEnabled(true);
                    etInputNumber.setEnabled(true);
                    etInputNumber.setText("");
                    etAb.setVisibility(View.VISIBLE);
                    etInputNumber.setVisibility(View.VISIBLE);
                    etSet1.setVisibility(View.GONE);
                    etSet2.setVisibility(View.GONE);
                }
            }
        });
        cbSeries.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cbSeries.isChecked()) {
                    cbPair.setChecked(false);
                    cbSetsWithpair.setChecked(false);
                    cbSetsWithoutpair.setChecked(false);
                    cbSeries.setChecked(true);
//
                    selectedCheckbox = "series";
                    etAb.setEnabled(false);
                    etInputNumber.setText("");
                    etAb.setVisibility(View.GONE);
                    etInputNumber.setVisibility(View.GONE);
                    etSet1.setVisibility(View.VISIBLE);
                    etSet1.setHint("Start");
                    etSet1.setText("");
                    etSet2.setVisibility(View.VISIBLE);
                    etSet2.setHint("End");
                    etSet2.setText("");
                    etAmount.setText("");
                } else {

                    selectedCheckbox = "none";
                    etAb.setEnabled(true);
                    etInputNumber.setEnabled(true);
                    etInputNumber.setText("");
                    etAb.setVisibility(View.VISIBLE);
                    etInputNumber.setVisibility(View.VISIBLE);
                    etSet1.setVisibility(View.GONE);
                    etSet2.setVisibility(View.GONE);
                }
            }
        });
    }

    private void recyclerviewWorks() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        GridLayoutManager layoutManager = new GridLayoutManager(this, 10);
        recyclerView.setLayoutManager(layoutManager);

        listinfo.clear();
        for (int dummy = 1; dummy <= 120; dummy++) {
            ResponseSheetByIdInfoNoArray rrDummy = new ResponseSheetByIdInfoNoArray();
            rrDummy.setNo(String.valueOf(dummy));
            rrDummy.setAmount(String.valueOf(0));
            listinfo.add(rrDummy);
        }
        adapterEditSheet = new AdapterEditSheet(listinfo, listPerNumTotal);
        recyclerView.setAdapter(adapterEditSheet);
    }


    @Override
    public void onClick(View view) {
        if (view == tvGo) {
            if (selectedCheckbox.equals("pair")) {
                if (TextUtils.isEmpty(etAmount.getText().toString())) {
                    etAmount.setError("enter amount");
                    Toast.makeText(this, "enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    int[] pairnum = {11, 22, 33, 44, 55, 66, 77, 88, 99, 100};
                    for (int pos = 0; pos < pairnum.length; pos++) {
                        if (treeMap_NumAmount.containsKey(pairnum[pos])) {
                            treeMap_NumAmount.put(pairnum[pos], treeMap_NumAmount.get(pairnum[pos]) + "," + etAmount.getText().toString().trim());
                        } else {
                            treeMap_NumAmount.put(pairnum[pos], etAmount.getText().toString().trim());
                        }

                    }

                    etAmount.setText("");
                    upDateLists(treeMap_NumAmount);
                    returnPerNumTotal(listinfo);
                    adapterEditSheet.notifyDataSetChanged();
                    Toast.makeText(this, "Pair Bet Successfullly added", Toast.LENGTH_SHORT).show();

                }

            }
            if (selectedCheckbox.equals("setswithpair")) {
                if (TextUtils.isEmpty(etSet1.getText().toString())) {
                    etSet1.setError("enter set1");
                    Toast.makeText(this, "enter set1", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etSet2.getText().toString())) {
                    etSet2.setError("enter set2");
                    Toast.makeText(this, "enter set2", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etAmount.getText().toString())) {
                    etAmount.setError("enter amount");
                    Toast.makeText(this, "enter amount", Toast.LENGTH_SHORT).show();
                } else {

                    TreeSet<Integer> treeadd = new TreeSet<Integer>();
                    String stringset1 = etSet1.getText().toString().trim();
                    String stringset2 = etSet2.getText().toString().trim();
                    for (int x = 0; x < stringset1.length(); x++) {
                        for (int y = 0; y < stringset2.length(); y++) {

                            String strnum = String.valueOf(stringset1.charAt(x)) + String.valueOf(stringset2.charAt(y));
                            int num = Integer.valueOf(strnum);
                            treeadd.add(num);
                            System.out.println("Last num " + num + "--- strnum-- " + strnum);

                        }
                    }
                    Iterator<Integer> itrr = treeadd.iterator();
                    while (itrr.hasNext()) {
                        int num = itrr.next();
                        if (treeMap_NumAmount.containsKey(num)) {
                            System.out.println("Lastcontains contains: " + num);
                            treeMap_NumAmount.put(num, treeMap_NumAmount.get(num) + "," + etAmount.getText().toString().trim());
                        } else {
                            System.out.println("Lastcontains  contains not: " + num);
                            treeMap_NumAmount.put(num, etAmount.getText().toString().trim());
                        }
                    }


                    etSet1.setText("");
                    etSet2.setText("");
                    etAmount.setText("");
                    upDateLists(treeMap_NumAmount);
                    returnPerNumTotal(listinfo);
                    adapterEditSheet.notifyDataSetChanged();
                    Toast.makeText(this, "Set with Pair Bet Successfullly added", Toast.LENGTH_SHORT).show();
                }

            }
            if (selectedCheckbox.equals("setswithoutpair")) {
                if (TextUtils.isEmpty(etSet1.getText().toString())) {
                    etSet1.setError("enter set1");
                    Toast.makeText(this, "enter set1", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etSet2.getText().toString())) {
                    etSet2.setError("enter set2");
                    Toast.makeText(this, "enter set2", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etAmount.getText().toString())) {
                    etAmount.setError("enter amount");
                    Toast.makeText(this, "enter amount", Toast.LENGTH_SHORT).show();
                } else {

                    TreeSet<Integer> treeadd = new TreeSet<Integer>();
                    String stringset1 = etSet1.getText().toString().trim();
                    String stringset2 = etSet2.getText().toString().trim();
                    for (int x = 0; x < stringset1.length(); x++) {
                        for (int y = 0; y < stringset2.length(); y++) {

                            if (!(String.valueOf(stringset1.charAt(x)).equals(String.valueOf(stringset2.charAt(y))))) {
                                String strnum = String.valueOf(stringset1.charAt(x)) + String.valueOf(stringset2.charAt(y));
                                int num = Integer.valueOf(strnum);
                                treeadd.add(num);
                                System.out.println("kamalnumbersarerr  num " + num + "--- strnum-- " + strnum);
                            }


                        }
                    }
                    Iterator<Integer> itrr = treeadd.iterator();
                    while (itrr.hasNext()) {
                        int num = itrr.next();
                        if (treeMap_NumAmount.containsKey(num)) {
                            System.out.println("kamalnumbersarerr contains: " + num);
                            treeMap_NumAmount.put(num, treeMap_NumAmount.get(num) + "," + etAmount.getText().toString().trim());
                        } else {
                            System.out.println("kamalnumbersarerr  contains not: " + num);
                            treeMap_NumAmount.put(num, etAmount.getText().toString().trim());
                        }
                    }


                    etSet1.setText("");
                    etSet2.setText("");
                    etAmount.setText("");
                    upDateLists(treeMap_NumAmount);
                    returnPerNumTotal(listinfo);
                    adapterEditSheet.notifyDataSetChanged();
                    Toast.makeText(this, "Set without Pair Bet Successfullly added", Toast.LENGTH_SHORT).show();
                }

            }
            if (selectedCheckbox.equals("series")) {
                if (TextUtils.isEmpty(etSet1.getText().toString())) {
                    etSet1.setError("enter set1");
                    Toast.makeText(this, "enter set1", Toast.LENGTH_SHORT).show();
                } else if (etSet1.getText().toString().equalsIgnoreCase("0")) {
                    etSet1.setError("enter start num");
                    Toast.makeText(this, "enter start num", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etSet2.getText().toString())) {
                    etSet2.setError("enter set2");
                    Toast.makeText(this, "enter set2", Toast.LENGTH_SHORT).show();
                } else if (etSet2.getText().toString().equalsIgnoreCase("0")) {
                    etSet1.setError("enter end num");
                    Toast.makeText(this, "enter end num", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etAmount.getText().toString())) {
                    etAmount.setError("enter amount");
                    Toast.makeText(this, "enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    int startnum = Integer.parseInt(etSet1.getText().toString());
                    int endnum = Integer.parseInt(etSet2.getText().toString());
                    for (int num = startnum; num <= endnum; num++) {
                        if (treeMap_NumAmount.containsKey(num)) {
                            treeMap_NumAmount.put(num, treeMap_NumAmount.get(num) + "," + etAmount.getText().toString().trim());
                        } else {
                            treeMap_NumAmount.put(num, etAmount.getText().toString().trim());
                        }

                    }
                    etSet1.setText("");
                    etSet2.setText("");
                    etAmount.setText("");

                    upDateLists(treeMap_NumAmount);
                    returnPerNumTotal(listinfo);
                    adapterEditSheet.notifyDataSetChanged();
                    Toast.makeText(this, "Series Bet Successfullly added", Toast.LENGTH_SHORT).show();
                }

            }
            if (selectedCheckbox.equals("none")) {
                if (TextUtils.isEmpty(etInputNumber.getText().toString())) {
                    etInputNumber.setError("enter number");
                    Toast.makeText(this, "enter number", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etAmount.getText().toString())) {
                    etAmount.setError("enter amount");
                    Toast.makeText(this, "enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    if (etAb.getText().toString().equalsIgnoreCase("")) {

                        TreeSet<Integer> treeAB = new TreeSet<Integer>();
                        String stringInpurNUm = etInputNumber.getText().toString().trim();
                        String arrNum[] = stringInpurNUm.split("-");

                        if (arrNum[arrNum.length - 1].length() != 2) {
                            Toast.makeText(this, "Wrong Numbers Pair.", Toast.LENGTH_SHORT).show();
                        } else {
                            for (int a = 0; a < arrNum.length; a++) {

                                int num = Integer.parseInt(String.format("%01d", Integer.valueOf(arrNum[a])));
                                if (num != 0) {
                                    treeAB.add(num);
                                }

                            }

                            Iterator<Integer> itrrAb = treeAB.iterator();
                            while (itrrAb.hasNext()) {
                                int num = itrrAb.next();
                                if (treeMap_NumAmount.containsKey(num)) {
                                    System.out.println("kamalnumbersarerr contains: " + num);
                                    treeMap_NumAmount.put(num, treeMap_NumAmount.get(num) + "," + etAmount.getText().toString().trim());
                                } else {
                                    System.out.println("kamalnumbersarerr  contains not: " + num);
                                    treeMap_NumAmount.put(num, etAmount.getText().toString().trim());
                                }
                            }


                            etAb.setText("");
                            etInputNumber.setText("");
                            etAmount.setText("");
                            upDateLists(treeMap_NumAmount);
                            returnPerNumTotal(listinfo);
                            adapterEditSheet.notifyDataSetChanged();
                            Toast.makeText(this, "Bet Successfullly added", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        TreeSet<Integer> treeAB = new TreeSet<Integer>();
                        String stringAB = etAb.getText().toString().trim();
                        String stringInpurNUm = etInputNumber.getText().toString().trim();

                        if (stringAB.equalsIgnoreCase("a")) {
                            for (int a = 0; a < stringInpurNUm.length(); a++) {
                                String strnum;
                                if (String.valueOf(stringInpurNUm.charAt(a)).equals("0")) {
                                    strnum = "12" + String.valueOf(stringInpurNUm.charAt(a));
                                } else {
                                    strnum = "11" + String.valueOf(stringInpurNUm.charAt(a));
                                }
                                int num = Integer.valueOf(strnum);
                                treeAB.add(num);
                                System.out.println("kamalnumbersarerr  andar " + num + "--- strnum-- " + strnum);
                            }
                        }
                        if (stringAB.equalsIgnoreCase("b")) {
                            for (int b = 0; b < stringInpurNUm.length(); b++) {
                                String strnum;
                                if (String.valueOf(stringInpurNUm.charAt(b)).equals("0")) {
                                    strnum = "11" + String.valueOf(stringInpurNUm.charAt(b));
                                } else {
                                    strnum = "10" + String.valueOf(stringInpurNUm.charAt(b));
                                }
                                int num = Integer.valueOf(strnum);
                                treeAB.add(num);
                                System.out.println("kamalnumbersarerr  bahar " + num + "--- strnum-- " + strnum);
                            }
                        }
                        if (stringAB.equalsIgnoreCase("ab") || stringAB.equalsIgnoreCase("ba")) {
                            for (int a = 0; a < stringInpurNUm.length(); a++) {
                                String strnum;
                                if (String.valueOf(stringInpurNUm.charAt(a)).equals("0")) {
                                    strnum = "12" + String.valueOf(stringInpurNUm.charAt(a));
                                } else {
                                    strnum = "11" + String.valueOf(stringInpurNUm.charAt(a));
                                }
                                int num = Integer.valueOf(strnum);
                                treeAB.add(num);
                                System.out.println("kamalnumbersarerr  andarbahar " + num + "--- strnum-- " + strnum);
                            }
                            for (int b = 0; b < stringInpurNUm.length(); b++) {
                                String strnum;
                                if (String.valueOf(stringInpurNUm.charAt(b)).equals("0")) {
                                    strnum = "11" + String.valueOf(stringInpurNUm.charAt(b));
                                } else {
                                    strnum = "10" + String.valueOf(stringInpurNUm.charAt(b));
                                }
                                int num = Integer.valueOf(strnum);
                                treeAB.add(num);
                                System.out.println("kamalnumbersarerr  baharandar " + num + "--- strnum-- " + strnum);
                            }
                        }

                        Iterator<Integer> itrrAb = treeAB.iterator();
                        while (itrrAb.hasNext()) {
                            int num = itrrAb.next();
                            if (treeMap_NumAmount.containsKey(num)) {
                                System.out.println("kamalnumbersarerr contains: " + num);
                                treeMap_NumAmount.put(num, treeMap_NumAmount.get(num) + "," + etAmount.getText().toString().trim());
                            } else {
                                System.out.println("kamalnumbersarerr  contains not: " + num);
                                treeMap_NumAmount.put(num, etAmount.getText().toString().trim());
                            }
                        }


                        etAb.setText("");
                        etInputNumber.setText("");
                        etAmount.setText("");
                        upDateLists(treeMap_NumAmount);
                        returnPerNumTotal(listinfo);
                        adapterEditSheet.notifyDataSetChanged();
                        Toast.makeText(this, "Andar Bahar Bet Successfullly added", Toast.LENGTH_SHORT).show();
                    }


                }
            }


        }
        if (view == tvSubmit) {
//            Iterator<Integer> iterator = treeMap_NumAmount.keySet().iterator();
//            while (iterator.hasNext()) {
//                Integer key = iterator.next();
//                System.out.println("ganeshajithanksji submit   Key: " + key + ", Value: " + treeMap_NumAmount.get(key));
//            }
            requestForGetSheetAddOrUpdate(user_id, sheet_id);
        }
    }

    private void upDateLists(TreeMap<Integer, String> treeMap) {
        for (int test = 0; test < listinfo.size(); test++) {
            Iterator<Integer> iterator = treeMap.keySet().iterator();
            while (iterator.hasNext()) {
                Integer key = iterator.next();
                if (listinfo.get(test).getNo().equals(String.valueOf(key))) {

                    System.out.println("heyganesha matches value list:"+listinfo.get(test).getNo()+"     input value:"+key);
//                    ResponseSheetByIdInfoNoArray rr = new ResponseSheetByIdInfoNoArray();
//                    rr.setNo(String.valueOf(key));
//                    rr.setAmount(treeMap.get(key));
//                    listinfo.add(test,rr);
                    listinfo.get(test).setAmount(treeMap.get(key));
                }

            }
        }


//        listinfo.clear();
//        Iterator<Integer> iterator = treeMap.keySet().iterator();
//        while (iterator.hasNext()) {
//            Integer key = iterator.next();
//            ResponseSheetByIdInfoNoArray rr = new ResponseSheetByIdInfoNoArray();
//            rr.setNo(String.valueOf(key));
//            rr.setAmount(treeMap.get(key));
//            listinfo.add(rr);
//        }
//        for (int k = 0; k < listinfo.size(); k++) {
//            System.out.println("ganeshajithanksji listupdated   Key: " + listinfo.get(k).getNo() + ", Value: " + listinfo.get(k).getAmount());
//        }

    }

    private void returnPerNumTotal(List<ResponseSheetByIdInfoNoArray> list) {

        int totalAmount = 0;
        listPerNumTotal.clear();
        for (ResponseSheetByIdInfoNoArray amt : list) {
            System.out.println("ganeshajithanksji returnPerNumTotal"+amt.getNo()+"======="+amt.getAmount());
            String[] stramount = amt.getAmount().split(",");
            int PerNumTotal = 0;
            for (int i = 0; i < stramount.length; i++) {
                PerNumTotal = PerNumTotal + (Integer.parseInt(stramount[i]));
            }
            listPerNumTotal.add(PerNumTotal);
            totalAmount = totalAmount + PerNumTotal;
        }
        tvTotal.setText(String.valueOf(totalAmount));
        for (int k = 0; k < listPerNumTotal.size(); k++) {
//            System.out.println("ganeshajithanksji listupdated  totalAmount: " + totalAmount + " number per:" + listPerNumTotal.get(k));
        }
    }

    private void dialogOpeningApp() {
        dialogLoading = new Dialog(ContractorAddSheet.this);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void upDateTreeMap(List<ResponseSheetByIdInfoNoArray> listnumamount) {
        treeMap_NumAmount = new TreeMap<>();
        for (int xx = 0; xx < listnumamount.size(); xx++) {
            treeMap_NumAmount.put(Integer.valueOf(listnumamount.get(xx).getNo()), listnumamount.get(xx).getAmount());
        }

    }


    private void requestForGetSheetAddOrUpdate(String user_id, String sheet_id) {
        try {

//           String jsonString="{\"sheet_total_amount\":\"360\",\"sheet_id\":\"64\",\"window_id\":\"3\",\"window_date\":\"2019-08-31\",\"user_id\":\"11\",\"no_1\":[100,80],\"no_2\":[100,80]}";
            String jsonString = "{\"sheet_total_amount\":\"" + tvTotal.getText().toString() + "\",\"sheet_id\":\"" + sheet_id + "\",\"window_id\":\"" + window_id + "\",\"window_date\":\"" + tvBookingdate.getText().toString() + "\",\"user_id\":\"" + user_id + "\"";


            String strNumData = "";
            Iterator<Integer> iterator = treeMap_NumAmount.keySet().iterator();
            while (iterator.hasNext()) {
                Integer key = iterator.next();
                strNumData = strNumData + "," + "\"no_" + key + "\":[" + treeMap_NumAmount.get(key) + "]";
            }
            jsonString = jsonString + strNumData + "}";


            JSONObject request = new JSONObject(jsonString);
            Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate request : " + request);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_ADDORUPDATESHEET, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseSheetAddedOrUpdated responseSheetAddedOrUpdated = gson.fromJson(response.toString(), ResponseSheetAddedOrUpdated.class);
                                Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate getMessage : " + responseSheetAddedOrUpdated.getMessage());
                                Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate getError : " + responseSheetAddedOrUpdated.getError());

                                if (responseSheetAddedOrUpdated.getError().equalsIgnoreCase("false")) {
                                    Toast.makeText(ContractorAddSheet.this, responseSheetAddedOrUpdated.getMessage(), Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(ContractorAddSheet.this, responseSheetAddedOrUpdated.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Logger.logDebug("kamal", "requestForGetSheetAddOrUpdate response null : ");
                                Toast.makeText(ContractorAddSheet.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Logger.logDebug("kamal", "requestForGetSheetAddOrUpdate catch  ");
                            Toast.makeText(ContractorAddSheet.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(ContractorAddSheet.this, MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_ADDORUPDATESHEET);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "requestForGetSheetAddOrUpdate OuterException e : " + e.getLocalizedMessage());
            Toast.makeText(ContractorAddSheet.this, MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    //start timer function
    void startTimer(final long timeDIffMilisinfuture) {
        cTimer = new CountDownTimer(timeDIffMilisinfuture, 1000) {
            public void onTick(long millis) {
//                long millis = 3600000;
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                if (timeDIffMilisinfuture < 43200000) {
                    rlTimer.setVisibility(View.VISIBLE);
                    tvTimerRemaining.setText(String.valueOf(hms));
                } else {
                    rlTimer.setVisibility(View.GONE);
                }
            }

            public void onFinish() {
                llEditor.setVisibility(View.GONE);
                rlTimer.setVisibility(View.VISIBLE);
                tvTimerRemaining.setText("Times Out");
            }
        };
        cTimer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    //cancel timer
    void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }
}
