package com.sattamatka.bettingmela.activity;

public interface SheetsClickListener {
    public void onSheetClicked(int position);
    public void onSheetClickDelete(int position);
}
