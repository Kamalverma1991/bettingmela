package com.sattamatka.bettingmela.utils;

import android.util.Log;

/**
 * Created by pradeep on 1/2/17.
 */

public class Logger {

    static boolean isLogEnabled = true;


    public static void logVerbose(String tag, String messsage) {
        if (isLogEnabled)
            Log.v(tag, messsage);
    }

    public static void logDebug(String tag, String messsage) {
        if (isLogEnabled)
            Log.d(tag, messsage);
    }

    public static void logWarn(String tag, String messsage) {
        if (isLogEnabled)
            Log.w(tag, messsage);
    }

    public static void logInfo(String tag, String messsage) {
        if (isLogEnabled)
            Log.i(tag, messsage);
    }
}
