package com.sattamatka.bettingmela.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Kamal Verma on 1/21/2016.
 */
public class MyPrefs {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEditor;
    private final String DATABASE_NAME = "mystorage";
    private final String LOGINCONTRACTOR = " Logincontractor";
    private final String ID = " id";
    private final String MOBILE = " mobile";
    private final String OTP = " otp";
    private final String AMOUNT = " amount";
    private final String USERNAME = " username";
    private final String EMAIL = " email";

    String amount;
    String username;
    String email;

    public MyPrefs(Context context) {
        sharedPreferences = context.getSharedPreferences(DATABASE_NAME, Context.MODE_PRIVATE);
    }


    public boolean isLogincontractor() {
        return sharedPreferences.getBoolean(LOGINCONTRACTOR, false);
    }

    public void setLogincontractor(boolean logincontractor) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(LOGINCONTRACTOR, logincontractor);
        spEditor.commit();
    }


    public String getOtp() {
        return sharedPreferences.getString(OTP, "");
    }

    public void setOtp(String otp) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(OTP, otp);
        spEditor.commit();
    }

    public String getId() {
        return sharedPreferences.getString(ID, "");
    }

    public void setId(String id) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(ID, id);
        spEditor.commit();
    }

    public String getMobile() {
        return sharedPreferences.getString(MOBILE, "");
    }

    public void setMobile(String mobile) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(MOBILE, mobile);
        spEditor.commit();
    }

    public String getAmount() {
        return sharedPreferences.getString(AMOUNT, "");
    }

    public void setAmount(String amount) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(AMOUNT, amount);
        spEditor.commit();
    }

    public String getUsername() {
        return sharedPreferences.getString(USERNAME, "");
    }

    public void setUsername(String username) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USERNAME, username);
        spEditor.commit();
    }

    public String getEmail() {
        return sharedPreferences.getString(EMAIL, "");
    }

    public void setEmail(String email) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(EMAIL, email);
        spEditor.commit();
    }
}

