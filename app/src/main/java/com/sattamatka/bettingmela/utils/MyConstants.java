package com.sattamatka.bettingmela.utils;

public class MyConstants {


    public static final String KEY_IS_LOGGED_IN = "key_login";
    public static final String KEY_USER_DATA = "key_stored_user_data";

    //services//
    public static final String BASE_URL = "http://nisee.com/bm/apis/v1/";
    public static final String URL_CONTRACTOR_LOGIN = BASE_URL + "contractor/login";
    public static final String URL_CONTRACTOR_CHECKUSEREXIST = BASE_URL + "contractor/checkUserExists";
    public static final String URL_CONTRACTOR_FORGOT_PASSWORD = BASE_URL + "contractor/forgotPassword";
    public static final String URL_CONTRACTOR_RESET_PASSWORD = BASE_URL + "contractor/resetPassword";
    public static final String URL_CONTRACTOR_GETDASHBOARDDATA = BASE_URL + "contractor/getDashboardData";
    public static final String URL_CONTRACTOR_GETMYTRANSACTION = BASE_URL + "contractor/getMyTXN";
    public static final String URL_CONTRACTOR_GETSHEETS = BASE_URL + "contractor/getSheets";
    public static final String URL_CONTRACTOR_GETSHEETBYID = BASE_URL + "contractor/getSheetById";
    public static final String URL_CONTRACTOR_DELETESHEET = BASE_URL + "contractor/deleteSheet";
    public static final String URL_CONTRACTOR_ADDORUPDATESHEET = BASE_URL + "contractor/submitSheet";
    public static final String URL_CONTRACTOR_GETUSERS = BASE_URL + "contractor/getUsers";
    public static final String URL_CONTRACTOR_ADDUSER = BASE_URL + "contractor/addUser";
    public static final String URL_CONTRACTOR_EDITUSER = BASE_URL + "contractor/editUser";
    public static final String URL_CONTRACTOR_DEBITUSERMONEY = BASE_URL + "contractor/debitMoneyOtp";
    public static final String URL_CONTRACTOR_ADDUSERMONEY = BASE_URL + "contractor/addUserMoney";
    public static final String URL_CONTRACTOR_GETUSERSTRANSACTION = BASE_URL + "contractor/getUsersTXN";

    //services//


    public static final String GOOGLE_PROJ_ID = "893448851715"; // Used For gcm


    ///toast messages//
    public static final String MES_OOPS = "Sorry,Your Request not found,Please try again after Sometime";
///toast messages//

}
