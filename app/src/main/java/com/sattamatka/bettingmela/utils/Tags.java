package com.sattamatka.bettingmela.utils;


public class Tags {


    public static final String TAG_LOGIN = "login";
    public static final String TAG_CHECKUSEREXISTS = "checkUserExists";
    public static final String TAG_FORGOTPASSWORD = "forgotpassword";
    public static final String TAG_RESETPASSWORD = "resetpassword";
    public static final String TAG_GETUSERS = "getusers";
    public static final String TAG_ADDUSER = "adduser";
    public static final String TAG_EDITUSER = "edituser";
    public static final String TAG_DEBITMONEYOTP = "debitmoneyotp";
    public static final String TAG_ADDUSERMONEY = "addusermoney";
    public static final String TAG_GETMYTRANSACTIONS = "getmytransactions";
    public static final String TAG_GETUSERSTRANSACTIONS = "getuserstransactions";
    public static final String TAG_GETSHEETS = "getsheets";
    public static final String TAG_GETSHEETBYID = "getsheetbyid";
    public static final String TAG_DELETESHEET = "deletesheet";
    public static final String TAG_ADDORUPDATESHEET = "addorupdatesheet";

    public static final String TAG_REGISTER = "register";
    public static final String TAG_SMS = "sms";
    public static final String TAG_UPDATEPASSWORD = "updatepassword";
    public static final String TAG_REFERRER_DETAILS = "referrerdetails";
    public static final String TAG_USER_DETAILS = "userdetails";
    public static final String TAG_GET_OPERATORS = "getoperators";
    public static final String TAG_MOBILE_RECHARGE = "mobile_recharge";
    public static final String TAG_CREDITUSER = "credituserpb";
}
