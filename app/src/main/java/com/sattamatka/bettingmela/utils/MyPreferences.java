package com.sattamatka.bettingmela.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by user on 6/7/16.
 */
public class MyPreferences {

    private static MyPreferences myPreferences;
    private SharedPreferences preferences;

    public MyPreferences(Context context, String key) {
        preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
    }

    public static MyPreferences getInstance(Context context) {
        if (myPreferences == null)
            myPreferences = new MyPreferences(context, context.getPackageName());
        return myPreferences;
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    public int getInt(String key, int defaultValue) {
        return preferences.getInt(key, defaultValue);
    }


    public long getLong(String key, long defaultValue) {
        return preferences.getLong(key, defaultValue);
    }

    public float getFloat(String key, float defaultValue) {
        return preferences.getFloat(key, defaultValue);
    }

    public void savePref(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
    }

    public void savePref(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }

    public void savePref(String key, String value) {
        preferences.edit().putString(key, value).commit();
    }

    public void savePref(String key, long value) {
        preferences.edit().putLong(key, value).commit();
    }

    public void savePref(String key, float value) {
        preferences.edit().putFloat(key, value).commit();
    }

    /**
     * Get Google advertise ID
     *
     * @return
     */
    public String getGaid() {
        return preferences.getString(UsefullUtils.GAID, null);
    }


}
