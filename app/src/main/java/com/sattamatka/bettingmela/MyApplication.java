package com.sattamatka.bettingmela;

import android.app.Application;
import android.content.Context;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.model.response.BaseResponse;
import com.sattamatka.bettingmela.utils.MyPreferences;
import static com.sattamatka.bettingmela.utils.MyConstants.KEY_USER_DATA;

public class MyApplication extends Application {
    private static MyApplication myApplication;
    private BaseResponse baseResponse;
    private MyPreferences myPreferences = null;


    public static MyApplication getInstance() {
        return myApplication;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = this;
        myPreferences = MyPreferences.getInstance(getApplicationContext());


    }

    public BaseResponse getBaseResponse() {
        return baseResponse;
    }

    public void setBaseResponse(BaseResponse baseResponse) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(baseResponse);
        myPreferences.savePref(KEY_USER_DATA, jsonString);
        this.baseResponse = baseResponse;
    }


}
