package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseSheetInfoDetail  implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("window_id")
    private String window_id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("contractor_id")
    private String contractor_id;
    @SerializedName("amount")
    private String amount;
    @SerializedName("total_no_bet")
    private String total_no_bet;
    @SerializedName("no_won")
    private String no_won;
    @SerializedName("total_profit")
    private String total_profit;
    @SerializedName("no_array")
    private String no_array;
    @SerializedName("window_name")
    private String window_name;
    @SerializedName("window_date")
    private String window_date;
    @SerializedName("modified_on")
    private String modified_on;
    @SerializedName("added_on")
    private String added_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWindow_id() {
        return window_id;
    }

    public void setWindow_id(String window_id) {
        this.window_id = window_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContractor_id() {
        return contractor_id;
    }

    public void setContractor_id(String contractor_id) {
        this.contractor_id = contractor_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal_no_bet() {
        return total_no_bet;
    }

    public void setTotal_no_bet(String total_no_bet) {
        this.total_no_bet = total_no_bet;
    }

    public String getNo_won() {
        return no_won;
    }

    public void setNo_won(String no_won) {
        this.no_won = no_won;
    }

    public String getTotal_profit() {
        return total_profit;
    }

    public void setTotal_profit(String total_profit) {
        this.total_profit = total_profit;
    }

    public String getNo_array() {
        return no_array;
    }

    public void setNo_array(String no_array) {
        this.no_array = no_array;
    }

    public String getWindow_name() {
        return window_name;
    }

    public void setWindow_name(String window_name) {
        this.window_name = window_name;
    }

    public String getWindow_date() {
        return window_date;
    }

    public void setWindow_date(String window_date) {
        this.window_date = window_date;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }
}
