package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseSheetByIdInfoNoArray {

    @SerializedName("no")
    private String no;
    @SerializedName("amount")
    private String amount;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
