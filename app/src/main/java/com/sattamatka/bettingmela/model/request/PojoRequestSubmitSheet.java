package com.sattamatka.bettingmela.model.request;

import com.google.gson.annotations.SerializedName;

public class PojoRequestSubmitSheet {
    @SerializedName("sheet_total_amount")
    private String sheet_total_amount;
    @SerializedName("sheet_id")
    private String sheet_id;
    @SerializedName("window_id")
    private String window_id;
    @SerializedName("window_date")
    private String window_date;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("no_1[]")
    private String no_1;
    @SerializedName("no_2[]")
    private String no_2;


    public String getSheet_total_amount() {
        return sheet_total_amount;
    }

    public void setSheet_total_amount(String sheet_total_amount) {
        this.sheet_total_amount = sheet_total_amount;
    }

    public String getSheet_id() {
        return sheet_id;
    }

    public void setSheet_id(String sheet_id) {
        this.sheet_id = sheet_id;
    }

    public String getWindow_id() {
        return window_id;
    }

    public void setWindow_id(String window_id) {
        this.window_id = window_id;
    }

    public String getWindow_date() {
        return window_date;
    }

    public void setWindow_date(String window_date) {
        this.window_date = window_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNo_1() {
        return no_1;
    }

    public void setNo_1(String no_1) {
        this.no_1 = no_1;
    }

    public String getNo_2() {
        return no_2;
    }

    public void setNo_2(String no_2) {
        this.no_2 = no_2;
    }
}
