package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseSheetById {
    @SerializedName("error")
    private String error;
    @SerializedName("message")
    private String message;

    @SerializedName("sheet")
    private ResponseSheetByIdInfo sheet;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseSheetByIdInfo getSheet() {
        return sheet;
    }

    public void setSheet(ResponseSheetByIdInfo sheet) {
        this.sheet = sheet;
    }
}
