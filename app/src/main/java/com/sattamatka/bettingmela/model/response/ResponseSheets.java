package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseSheets {

    @SerializedName("error")
    private String error;
    @SerializedName("message")
    private String message;

    @SerializedName("sheets")
    private ResponseSheetInfo sheets;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseSheetInfo getSheets() {
        return sheets;
    }

    public void setSheets(ResponseSheetInfo sheets) {
        this.sheets = sheets;
    }
}
