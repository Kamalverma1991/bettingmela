package com.sattamatka.bettingmela.model;

import com.google.gson.annotations.SerializedName;

public class PojoRequestAddUserMoney {
    @SerializedName("money_type")
    private String money_type;
    @SerializedName("add_coin")
    private String add_coin;
    @SerializedName("contractor_money_id")
    private String contractor_money_id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("money_otp")
    private String money_otp;

    public String getMoney_type() {
        return money_type;
    }

    public void setMoney_type(String money_type) {
        this.money_type = money_type;
    }

    public String getAdd_coin() {
        return add_coin;
    }

    public void setAdd_coin(String add_coin) {
        this.add_coin = add_coin;
    }

    public String getContractor_money_id() {
        return contractor_money_id;
    }

    public void setContractor_money_id(String contractor_money_id) {
        this.contractor_money_id = contractor_money_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMoney_otp() {
        return money_otp;
    }

    public void setMoney_otp(String money_otp) {
        this.money_otp = money_otp;
    }
}
