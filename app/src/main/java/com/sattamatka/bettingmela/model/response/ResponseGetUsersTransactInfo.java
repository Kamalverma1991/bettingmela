package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetUsersTransactInfo {

    @SerializedName("total_records")
    private String total_records;
    @SerializedName("contractors")
    private List<ResponseGetUsersTransactInfoContractors> usertransactcontractors;

    public String getTotal_records() {
        return total_records;
    }

    public void setTotal_records(String total_records) {
        this.total_records = total_records;
    }


    public List<ResponseGetUsersTransactInfoContractors> getUsertransactcontractors() {
        return usertransactcontractors;
    }

    public void setUsertransactcontractors(List<ResponseGetUsersTransactInfoContractors> usertransactcontractors) {
        this.usertransactcontractors = usertransactcontractors;
    }
}
