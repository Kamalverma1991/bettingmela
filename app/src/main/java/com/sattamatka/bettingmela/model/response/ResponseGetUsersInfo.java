package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetUsersInfo {

    @SerializedName("total_records")
    private String total_records;
    @SerializedName("contractors")
    private List<ResponseGetUsersInfoContractors> contractors;


    public String getTotal_records() {
        return total_records;
    }

    public void setTotal_records(String total_records) {
        this.total_records = total_records;
    }

    public List<ResponseGetUsersInfoContractors> getContractors() {
        return contractors;
    }

    public void setContractors(List<ResponseGetUsersInfoContractors> contractors) {
        this.contractors = contractors;
    }


}
