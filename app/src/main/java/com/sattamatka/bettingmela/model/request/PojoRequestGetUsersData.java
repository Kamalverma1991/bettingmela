package com.sattamatka.bettingmela.model.request;

import com.google.gson.annotations.SerializedName;

public class PojoRequestGetUsersData {

    @SerializedName("per_page_record")
    private String per_page_record;
    @SerializedName("page_no")
    private String page_no;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("filter")
    private String filter;
    @SerializedName("type")
    private String type;
    @SerializedName("search_option")
    private String search_option;
    @SerializedName("search_text")
    private String search_text;

    public String getPer_page_record() {
        return per_page_record;
    }

    public void setPer_page_record(String per_page_record) {
        this.per_page_record = per_page_record;
    }

    public String getPage_no() {
        return page_no;
    }

    public void setPage_no(String page_no) {
        this.page_no = page_no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSearch_option() {
        return search_option;
    }

    public void setSearch_option(String search_option) {
        this.search_option = search_option;
    }

    public String getSearch_text() {
        return search_text;
    }

    public void setSearch_text(String search_text) {
        this.search_text = search_text;
    }
}
