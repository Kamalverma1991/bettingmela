package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;
import com.sattamatka.bettingmela.model.UserData;

/**
 * Project           : nc
 * File Name      : BaseResponse
 * Description    : TODO: Enter description
 * Revision History : version 1:
 * Date : 4/29/17
 * Original author : pradeep
 * Description : Initial version
 */
public class BaseResponse {
    @SerializedName("error")
    private String error;
    @SerializedName("message")
    private String message;


    @SerializedName("user")
    private UserData userData;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
