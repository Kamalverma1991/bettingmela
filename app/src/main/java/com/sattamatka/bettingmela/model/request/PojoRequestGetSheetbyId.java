package com.sattamatka.bettingmela.model.request;

import com.google.gson.annotations.SerializedName;

public class PojoRequestGetSheetbyId {
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("sheet_id")
    private String sheet_id;
    @SerializedName("request_from")
    private String request_from;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSheet_id() {
        return sheet_id;
    }

    public void setSheet_id(String sheet_id) {
        this.sheet_id = sheet_id;
    }

    public String getRequest_from() {
        return request_from;
    }

    public void setRequest_from(String request_from) {
        this.request_from = request_from;
    }
}
