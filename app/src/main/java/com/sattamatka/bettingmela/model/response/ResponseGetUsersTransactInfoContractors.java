package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetUsersTransactInfoContractors {



    @SerializedName("id")
    private String id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("contractor_id")
    private String contractor_id;

    @SerializedName("betting_id")
    private String betting_id;


    @SerializedName("withdraw_id")
    private String withdraw_id;

    @SerializedName("amount")
    private String amount;

    @SerializedName("total_amount")
    private String total_amount;

    @SerializedName("type")
    private String type;

    @SerializedName("remarks")
    private String remarks;

    @SerializedName("added_on")
    private String added_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContractor_id() {
        return contractor_id;
    }

    public void setContractor_id(String contractor_id) {
        this.contractor_id = contractor_id;
    }

    public String getBetting_id() {
        return betting_id;
    }

    public void setBetting_id(String betting_id) {
        this.betting_id = betting_id;
    }

    public String getWithdraw_id() {
        return withdraw_id;
    }

    public void setWithdraw_id(String withdraw_id) {
        this.withdraw_id = withdraw_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }
}
