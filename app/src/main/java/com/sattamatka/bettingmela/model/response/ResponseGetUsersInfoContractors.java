package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetUsersInfoContractors {


    @SerializedName("modified_on")
    private String modified_on;

    @SerializedName("amount")
    private String amount;

    @SerializedName("commission")
    private String commission;

    @SerializedName("cut")
    private String cut;

    @SerializedName("mobile_verify_status")
    private String mobile_verify_status;


    @SerializedName("os")
    private String os;

    @SerializedName("last_login")
    private String last_login;

    @SerializedName("ip")
    private String ip;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("otp")
    private String otp;

    @SerializedName("account_status")
    private String account_status;

    @SerializedName("password")
    private String password;

    @SerializedName("contractor_id")
    private String contractor_id;

    @SerializedName("api_key")
    private String api_key;

    @SerializedName("email_verify_code")
    private String email_verify_code;

    @SerializedName("id")
    private String id;

    @SerializedName("browser_detail")
    private String browser_detail;

    @SerializedName("email_verify_status")
    private String email_verify_status;

    @SerializedName("added_on")
    private String added_on;

    @SerializedName("email")
    private String email;

    @SerializedName("username")
    private String username;


    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public String getMobile_verify_status() {
        return mobile_verify_status;
    }

    public void setMobile_verify_status(String mobile_verify_status) {
        this.mobile_verify_status = mobile_verify_status;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContractor_id() {
        return contractor_id;
    }

    public void setContractor_id(String contractor_id) {
        this.contractor_id = contractor_id;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getEmail_verify_code() {
        return email_verify_code;
    }

    public void setEmail_verify_code(String email_verify_code) {
        this.email_verify_code = email_verify_code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrowser_detail() {
        return browser_detail;
    }

    public void setBrowser_detail(String browser_detail) {
        this.browser_detail = browser_detail;
    }

    public String getEmail_verify_status() {
        return email_verify_status;
    }

    public void setEmail_verify_status(String email_verify_status) {
        this.email_verify_status = email_verify_status;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
