package com.sattamatka.bettingmela.model.request;

import com.google.gson.annotations.SerializedName;

public class PojoRequestDebitMoneyOtp {

    @SerializedName("user_id")
    private String user_id;
    @SerializedName("contractor_id")
    private String contractor_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContractor_id() {
        return contractor_id;
    }

    public void setContractor_id(String contractor_id) {
        this.contractor_id = contractor_id;
    }
}
