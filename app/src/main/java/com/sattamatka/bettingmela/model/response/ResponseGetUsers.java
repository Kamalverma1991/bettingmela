package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetUsers {
    @SerializedName("error")
    private String error;
    @SerializedName("message")
    private String message;

    @SerializedName("contractors")
    private ResponseGetUsersInfo contractors;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseGetUsersInfo getContractors() {
        return contractors;
    }

    public void setContractors(ResponseGetUsersInfo contractors) {
        this.contractors = contractors;
    }
}
