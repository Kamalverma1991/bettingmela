package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseSheetInfo {

    @SerializedName("total_records")
    private String total_records;
    @SerializedName("sheet")
    private List<ResponseSheetInfoDetail> sheet;

    public String getTotal_records() {
        return total_records;
    }

    public void setTotal_records(String total_records) {
        this.total_records = total_records;
    }

    public List<ResponseSheetInfoDetail> getSheet() {
        return sheet;
    }

    public void setSheet(List<ResponseSheetInfoDetail> sheet) {
        this.sheet = sheet;
    }
}
