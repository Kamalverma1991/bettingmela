package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetDasboardUsers {

    @SerializedName("total")
    private String total;

    @SerializedName("total_users")
    private String total_users;

    @SerializedName("total_enabled")
    private String total_enabled;

    @SerializedName("total_disabled")
    private String total_disabled;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal_users() {
        return total_users;
    }

    public void setTotal_users(String total_users) {
        this.total_users = total_users;
    }

    public String getTotal_enabled() {
        return total_enabled;
    }

    public void setTotal_enabled(String total_enabled) {
        this.total_enabled = total_enabled;
    }

    public String getTotal_disabled() {
        return total_disabled;
    }

    public void setTotal_disabled(String total_disabled) {
        this.total_disabled = total_disabled;
    }
}
