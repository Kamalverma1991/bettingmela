package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetDasboardInfo {

    @SerializedName("user")
    private ResponseGetDasboardUsers user;
    @SerializedName("sheets")
    private ResponseGetDasboardSheets sheets;


    public ResponseGetDasboardUsers getUser() {
        return user;
    }

    public void setUser(ResponseGetDasboardUsers user) {
        this.user = user;
    }

    public ResponseGetDasboardSheets getSheets() {
        return sheets;
    }

    public void setSheets(ResponseGetDasboardSheets sheets) {
        this.sheets = sheets;
    }
}

