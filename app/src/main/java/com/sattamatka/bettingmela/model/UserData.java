package com.sattamatka.bettingmela.model;

import com.google.gson.annotations.SerializedName;

/**
 * Project           : DoseraNew
 * File Name         : UserData
 * Description       : TODO: Enter description
 * Revision History: version 1:
 * Date: 2/1/17
 * Original author: pradeep
 * Description: Initial version
 */




public class UserData {

    @SerializedName("id")
    private String id;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String mobile;

    @SerializedName("amount")
    private String amount;
    @SerializedName("email_verify_code")
    private String email_verify_code;
    @SerializedName("email_verify_status")
    private String email_verify_status;
    @SerializedName("otp")
    private String otp;
    @SerializedName("mobile_verify_status")
    private String mobile_verify_status;
    @SerializedName("api_key")
    private String api_key;
    @SerializedName("browser_detail")
    private String browser_detail;
    @SerializedName("account_status")
    private String account_status;




    @SerializedName("ip")
    private String ip;
    @SerializedName("browser")
    private String browser;
    @SerializedName("os")
    private String os;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEmail_verify_code() {
        return email_verify_code;
    }

    public void setEmail_verify_code(String email_verify_code) {
        this.email_verify_code = email_verify_code;
    }

    public String getEmail_verify_status() {
        return email_verify_status;
    }

    public void setEmail_verify_status(String email_verify_status) {
        this.email_verify_status = email_verify_status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMobile_verify_status() {
        return mobile_verify_status;
    }

    public void setMobile_verify_status(String mobile_verify_status) {
        this.mobile_verify_status = mobile_verify_status;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getBrowser_detail() {
        return browser_detail;
    }

    public void setBrowser_detail(String browser_detail) {
        this.browser_detail = browser_detail;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
