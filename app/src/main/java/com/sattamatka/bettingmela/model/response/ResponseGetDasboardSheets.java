package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetDasboardSheets {

    @SerializedName("total")
    private String total;

    @SerializedName("total_sheets")
    private String total_sheets;

    @SerializedName("total_no_bet")
    private String total_no_bet;

    @SerializedName("total_loss")
    private String total_loss;

    @SerializedName("total_profit")
    private String total_profit;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal_sheets() {
        return total_sheets;
    }

    public void setTotal_sheets(String total_sheets) {
        this.total_sheets = total_sheets;
    }

    public String getTotal_no_bet() {
        return total_no_bet;
    }

    public void setTotal_no_bet(String total_no_bet) {
        this.total_no_bet = total_no_bet;
    }

    public String getTotal_loss() {
        return total_loss;
    }

    public void setTotal_loss(String total_loss) {
        this.total_loss = total_loss;
    }

    public String getTotal_profit() {
        return total_profit;
    }

    public void setTotal_profit(String total_profit) {
        this.total_profit = total_profit;
    }
}
