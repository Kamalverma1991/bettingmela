package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetUsersTransact {

    @SerializedName("error")
    private String error;

    @SerializedName("message")
    private String message;

    @SerializedName("contractors")
    private ResponseGetUsersTransactInfo contractors;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseGetUsersTransactInfo getContractors() {
        return contractors;
    }

    public void setContractors(ResponseGetUsersTransactInfo contractors) {
        this.contractors = contractors;
    }
}
