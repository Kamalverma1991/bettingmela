package com.sattamatka.bettingmela.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseGetDashboarddata {

    @SerializedName("error")
    private String error;

    @SerializedName("message")
    private String message;

    @SerializedName("Dashbboard")
    private ResponseGetDasboardInfo Dashbboard;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseGetDasboardInfo getDashbboard() {
        return Dashbboard;
    }

    public void setDashbboard(ResponseGetDasboardInfo dashbboard) {
        Dashbboard = dashbboard;
    }
}
