package com.sattamatka.bettingmela.model.request;

import com.google.gson.annotations.SerializedName;

public class PojoRequestGetDashboardData {

    @SerializedName("user_id")
    private String user_id;
    @SerializedName("start_date")
    private String start_date;
    @SerializedName("end_date")
    private String end_date;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}
