package com.sattamatka.bettingmela.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.activity.SheetsClickListener;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransactInfoContractors;
import com.sattamatka.bettingmela.model.response.ResponseSheetInfoDetail;

import java.util.List;

public class AdapterSheets extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<ResponseSheetInfoDetail> list;
    SheetsClickListener sheetsClickListener;

    public AdapterSheets(List<ResponseSheetInfoDetail> list,SheetsClickListener sheetsClickListener) {
        this.list = list;
        this.sheetsClickListener = sheetsClickListener;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sheets, parent, false);
            return new AdapterSheets.MyViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new AdapterSheets.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof AdapterSheets.MyViewHolder) {
            populateItemRows((AdapterSheets.MyViewHolder) viewHolder, position);
        } else if (viewHolder instanceof AdapterSheets.LoadingViewHolder) {
            showLoadingView((AdapterSheets.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }






    public class MyViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout llmain;
        public TextView tvid;
        public TextView tvUserid;
        public TextView tvContractorid;
        public TextView tvTotalNobet;
        public TextView tvAmount;
        public TextView tvTotalProfit;
        public TextView tvWindowName;
        public TextView tvWindowDate;
        public TextView tvAddeddate;
        public ImageView ivEdit;
        public ImageView ivDelete;

        public MyViewHolder(View view) {
            super(view);
            llmain = (LinearLayout) view.findViewById(R.id.linear_main);
            tvid = (TextView) view.findViewById(R.id.textview_id);
            tvUserid = (TextView) view.findViewById(R.id.textview_userid);
            tvContractorid = (TextView) view.findViewById(R.id.textview_contractorid);
            tvTotalNobet = (TextView) view.findViewById(R.id.textview_totalnobet);
            tvAmount = (TextView) view.findViewById(R.id.textview_amount);
            tvTotalProfit = (TextView) view.findViewById(R.id.textview_totalprofit);
            tvWindowName = (TextView) view.findViewById(R.id.textview_windowname);
            tvWindowDate = (TextView) view.findViewById(R.id.textview_windowdate);
            tvAddeddate = (TextView) view.findViewById(R.id.textview_addeddate);
            ivEdit = (ImageView) view.findViewById(R.id.imageview_edit);
            ivDelete = (ImageView) view.findViewById(R.id.imageview_delete);
        }
    }
    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(AdapterSheets.LoadingViewHolder viewHolder, int position) {

    }

    private void populateItemRows(AdapterSheets.MyViewHolder holder, final int position) {
        if (position % 2 == 1)
            holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"));
        else
            holder.itemView.setBackgroundColor(Color.parseColor("#F1F1F1"));

        holder.tvid.setText(list.get(position).getId());
        holder.tvUserid.setText(list.get(position).getUser_id());
        holder.tvContractorid.setText(list.get(position).getContractor_id());
        holder.tvTotalNobet.setText(list.get(position).getTotal_no_bet());
        holder.tvAmount.setText(list.get(position).getAmount());
        holder.tvTotalProfit.setText(list.get(position).getTotal_profit());
        holder.tvWindowName.setText(list.get(position).getWindow_name());
        holder.tvWindowDate.setText(list.get(position).getWindow_date());
        holder.tvAddeddate.setText(list.get(position).getAdded_on());
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetsClickListener.onSheetClicked(position);
            }
        });
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetsClickListener.onSheetClickDelete(position);
            }
        });
    }
}
