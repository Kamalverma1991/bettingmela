package com.sattamatka.bettingmela.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.activity.SheetsClickListener;
import com.sattamatka.bettingmela.activity.UsersListClickListeners;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersInfoContractors;

import java.util.List;

public class AdapterUsers extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<ResponseGetUsersInfoContractors> list;
    UsersListClickListeners usersListClickListeners;

    public AdapterUsers(List<ResponseGetUsersInfoContractors> list,UsersListClickListeners usersListClickListeners) {
        this.list = list;
        this.usersListClickListeners = usersListClickListeners;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_users, parent, false);
            return new AdapterUsers.MyViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new AdapterUsers.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof AdapterUsers.MyViewHolder) {
            populateItemRows((AdapterUsers.MyViewHolder) viewHolder, position);
        } else if (viewHolder instanceof AdapterUsers.LoadingViewHolder) {
            showLoadingView((AdapterUsers.LoadingViewHolder) viewHolder, position);
        }

    }



    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView tvid;
        public TextView tvUserName;
        public TextView tvEmail;
        public TextView tvMobile;
        public TextView tvAmount;
        public ImageView ivUserCreditOrDebit;
        public ImageView ivEdit;

        public MyViewHolder(View view) {
            super(view);
            tvid = (TextView) view.findViewById(R.id.textview_id);
            tvUserName = (TextView) view.findViewById(R.id.textview_username);
            tvEmail = (TextView) view.findViewById(R.id.textview_email);
            tvMobile = (TextView) view.findViewById(R.id.textview_mobile);
            tvAmount = (TextView) view.findViewById(R.id.textview_amount);
            ivUserCreditOrDebit = (ImageView) view.findViewById(R.id.imageview_usercreditordebit);
            ivEdit = (ImageView) view.findViewById(R.id.imageview_edit);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(AdapterUsers.LoadingViewHolder viewHolder, int position) {

    }

    private void populateItemRows(AdapterUsers.MyViewHolder holder, final int position) {
        if(position %2 == 1)
            holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"));
        else
            holder.itemView.setBackgroundColor(Color.parseColor("#F1F1F1"));

        holder.tvid.setText(list.get(position).getId());
        holder.tvUserName.setText(list.get(position).getUsername());
        holder.tvEmail.setText(list.get(position).getEmail());
        holder.tvMobile.setText(list.get(position).getMobile());
        holder.tvAmount.setText(list.get(position).getAmount());
        holder.tvid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usersListClickListeners.onUsersIdClickForTransactions(position);
            }
        });
        holder.ivUserCreditOrDebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usersListClickListeners.onUsersCreditDebitAdd(position);
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usersListClickListeners.onUsersEdit(position);
            }
        });
    }
}
