package com.sattamatka.bettingmela.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.activity.SheetsClickListener;
import com.sattamatka.bettingmela.model.response.ResponseSheetByIdInfoNoArray;

import java.util.List;

public class AdapterEditSheet extends RecyclerView.Adapter<AdapterEditSheet.MyViewHolder> {

//    private List<ResponseSheetByIdInfoNoArray> listDummy;
    private List<ResponseSheetByIdInfoNoArray> list;
    private List<Integer> listPerNumTotal;

    public AdapterEditSheet( List<ResponseSheetByIdInfoNoArray> list, List<Integer> listPerNumTotal) {
//        this.listDummy = listDummy;
        this.list = list;
        this.listPerNumTotal = listPerNumTotal;
    }


    @NonNull
    @Override
    public AdapterEditSheet.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_editsheet, parent, false);

        return new AdapterEditSheet.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdapterEditSheet.MyViewHolder holder, final int position) {
        int sum = 0;
        int n = position;
        while (n != 0) {
            sum = sum + n % 10;
            n = n / 10;
        }
        if(position<=99){
            if (sum % 2 == 1)
                holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"));
             else
                holder.itemView.setBackgroundColor(Color.parseColor("#F1F1F1"));
        }else if(position>99){
            if (sum % 2 == 1)
            holder.itemView.setBackgroundColor(Color.parseColor("#F1F1F1"));
            else
                holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"));
        }

        try {
            holder.tvNum.setText(list.get(position).getNo());
            holder.tvAmount.setText(listPerNumTotal.get(position).toString());
        } catch (Exception e) {

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout llmain;
        public TextView tvNum;
        public TextView tvAmount;
//        public ImageView ivCancel;

        public MyViewHolder(View view) {
            super(view);
            llmain = (ConstraintLayout) view.findViewById(R.id.constraintlayout_main);
            tvNum = (TextView) view.findViewById(R.id.textview_num);
            tvAmount = (TextView) view.findViewById(R.id.textview_amount);
//            ivCancel = (ImageView) view.findViewById(R.id.imageview_cancel);

        }
    }
}
