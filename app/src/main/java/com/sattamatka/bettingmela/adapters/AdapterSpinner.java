package com.sattamatka.bettingmela.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.utils.MySpinner;

public class AdapterSpinner extends ArrayAdapter<String> {

    private String[] dateList;
    private MySpinner spin;

    public AdapterSpinner(Context context, int textViewResourceId, String[] dateList, MySpinner spin) {
        super(context, textViewResourceId, dateList);
        this.dateList = dateList;
        this.spin = spin;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        LayoutInflater inflater = LayoutInflater.from(prnt.getContext());
//        View spinnerItem = inflater.inflate(android.R.layout.simple_spinner_item, null);
        View spinnerItem = inflater.inflate(R.layout.spinner_item, null);

        TextView mytext = (TextView) spinnerItem.findViewById(android.R.id.text1);
        mytext.setText(dateList[position]);

        int selected = spin.getSelectedItemPosition();
        if (position == selected) {
            mytext.setTextColor(getContext().getResources().getColor(R.color.white));
            spinnerItem.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));
        }
        return spinnerItem;

    }

}