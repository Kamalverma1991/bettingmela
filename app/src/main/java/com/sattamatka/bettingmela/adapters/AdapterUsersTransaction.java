package com.sattamatka.bettingmela.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransactInfoContractors;

import java.util.List;

public class AdapterUsersTransaction extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<ResponseGetUsersTransactInfoContractors> list;

    public AdapterUsersTransaction(List<ResponseGetUsersTransactInfoContractors> list) {
        this.list = list;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_userstransaction, parent, false);
            return new MyViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof MyViewHolder) {
            populateItemRows((MyViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        }

    }



    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvid;
        public TextView tvUserid;
        public TextView tvBettingid;
//        public TextView tvWithdrawid;
        public TextView tvAmount;
        public TextView tvTotalamount;
        public TextView tvType;
        public TextView tvAddedon;

        public MyViewHolder(View view) {
            super(view);
            tvid = (TextView) view.findViewById(R.id.textview_id);
            tvUserid = (TextView) view.findViewById(R.id.textview_userid);
            tvBettingid = (TextView) view.findViewById(R.id.textview_bettingid);
//            tvWithdrawid = (TextView) view.findViewById(R.id.textview_withdrawid);
            tvAmount = (TextView) view.findViewById(R.id.textview_amount);
            tvTotalamount = (TextView) view.findViewById(R.id.textview_totalamount);
            tvType = (TextView) view.findViewById(R.id.textview_type);
            tvAddedon = (TextView) view.findViewById(R.id.textview_addedon);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {

    }

    private void populateItemRows(MyViewHolder holder, int position) {
        if(position %2 == 1)
            holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"));
        else
            holder.itemView.setBackgroundColor(Color.parseColor("#F1F1F1"));

        holder.tvid.setText(list.get(position).getId());
        holder.tvUserid.setText(list.get(position).getUser_id());
        holder.tvBettingid.setText(list.get(position).getBetting_id());
//        holder.tvWithdrawid.setText(list.get(position).getWithdraw_id());
        holder.tvAmount.setText(list.get(position).getAmount());
        holder.tvTotalamount.setText(list.get(position).getTotal_amount());
        holder.tvType.setText(list.get(position).getType());
        holder.tvAddedon.setText(list.get(position).getAdded_on());
    }

}
