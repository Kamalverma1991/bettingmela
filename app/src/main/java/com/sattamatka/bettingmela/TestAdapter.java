//package com.sattamatka.bettingmela.adapters;
//
//import android.graphics.Color;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.sattamatka.bettingmela.R;
//import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransactInfoContractors;
//
//import java.util.List;
//
//public class TestAdapter extends RecyclerView.Adapter<TestAdapter.MyViewHolder> {
//
//    private List<ResponseGetUsersTransactInfoContractors> list;
//
//    public TestAdapter(List<ResponseGetUsersTransactInfoContractors> list) {
//        this.list = list;
//    }
//
//
//    @NonNull
//    @Override
//    public TestAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.list_userstransaction, parent, false);
//
//        return new TestAdapter.MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(TestAdapter.MyViewHolder holder, int position) {
//        if (position % 2 == 1)
//            holder.itemView.setBackgroundColor(Color.parseColor("#F7F7F7"));
//        else
//            holder.itemView.setBackgroundColor(Color.parseColor("#F1F1F1"));
//
//        holder.tvid.setText(list.get(position).getId());
//        holder.tvUserid.setText(list.get(position).getUser_id());
//        holder.tvBettingid.setText(list.get(position).getBetting_id());
////        holder.tvWithdrawid.setText(list.get(position).getWithdraw_id());
//        holder.tvAmount.setText(list.get(position).getAmount());
//        holder.tvTotalamount.setText(list.get(position).getTotal_amount());
//        holder.tvType.setText(list.get(position).getType());
//        holder.tvAddedon.setText(list.get(position).getAdded_on());
//    }
//
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//
//        public TextView tvid;
//        public TextView tvUserid;
//        public TextView tvBettingid;
//        //        public TextView tvWithdrawid;
//        public TextView tvAmount;
//        public TextView tvTotalamount;
//        public TextView tvType;
//        public TextView tvAddedon;
//
//        public MyViewHolder(View view) {
//            super(view);
//            tvid = (TextView) view.findViewById(R.id.textview_id);
//            tvUserid = (TextView) view.findViewById(R.id.textview_userid);
//            tvBettingid = (TextView) view.findViewById(R.id.textview_bettingid);
////            tvWithdrawid = (TextView) view.findViewById(R.id.textview_withdrawid);
//            tvAmount = (TextView) view.findViewById(R.id.textview_amount);
//            tvTotalamount = (TextView) view.findViewById(R.id.textview_totalamount);
//            tvType = (TextView) view.findViewById(R.id.textview_type);
//            tvAddedon = (TextView) view.findViewById(R.id.textview_addedon);
//        }
//    }
//
//
//
//}









/////////////////////////////////////GragmentUserTransactions///////////////////////////////////////////////////////////////

//package com.sattamatka.bettingmela.fragment;
//
//import android.app.Dialog;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.google.gson.Gson;
//import com.sattamatka.bettingmela.R;
//import com.sattamatka.bettingmela.VolleyWrappers;
//import com.sattamatka.bettingmela.adapters.AdapterUsersTransaction;
//import com.sattamatka.bettingmela.model.request.PojoRequestGetUsersData;
//import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransact;
//import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransactInfoContractors;
//import com.sattamatka.bettingmela.utils.Logger;
//import com.sattamatka.bettingmela.utils.MyConstants;
//import com.sattamatka.bettingmela.utils.MyPrefs;
//
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
//import static com.sattamatka.bettingmela.utils.Tags.TAG_GETUSERSTRANSACTIONS;
//
//public class ContractorFragmentUserstransaction extends Fragment {
//
//    Dialog dialogLoading;
//    private MyPrefs myPrefs;
//    private VolleyWrappers volleyWrappers = null;
//
//    private List<ResponseGetUsersTransactInfoContractors> list = new ArrayList<>();
//    private RecyclerView recyclerView;
//    private AdapterUsersTransaction adapterUsersTransaction;
//    String[] users = {"Select", "ID", "User ID", "Betting ID", "Amount", "Type"};
//    Spinner spin;
//    private EditText etSearch;
//    private ImageView ivSearch;
//    private TextView tvResponse;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.contractor_fragment_usertransaction, container, false);
//    }
//
//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        myPrefs = new MyPrefs(getActivity());
//        volleyWrappers = new VolleyWrappers(getActivity());
//
//        spin = (Spinner) view.findViewById(R.id.spinner);
//        etSearch = view.findViewById(R.id.edittext_search);
//        ivSearch = view.findViewById(R.id.imageview_search);
//        tvResponse = view.findViewById(R.id.textview_response);
//        ivSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String searchoption = spin.getSelectedItem().toString().replace(" ", "_").toLowerCase();
//                String searchtext = etSearch.getText().toString();
//                if (searchoption.equals("Select")) {
//                    Toast.makeText(getActivity(), "Please select filter before search", Toast.LENGTH_SHORT).show();
//                } else if (TextUtils.isEmpty(searchtext)) {
//                    etSearch.setError("enter text");
//                    Toast.makeText(getActivity(), "Please enter search text", Toast.LENGTH_SHORT).show();
//                } else {
//                    requestForGetUsersTransaction("10", "1", myPrefs.getId(), "id", "desc", searchoption, searchtext);
//
//                }
//
//
//            }
//        });
//
//        recyclerView = view.findViewById(R.id.recycler_view_usertransaction);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//
//        adapterUsersTransaction = new AdapterUsersTransaction(list);
//        recyclerView.setAdapter(adapterUsersTransaction);
//        spinnerworks(view);
//        if (getArguments() != null) {
//            String searchoptionFromUserFrag = getArguments().getString("searchoption");
//            String searchtextFromUserFrag = getArguments().getString("searchtext");
//            spin.setSelection(2);
//            etSearch.setText(searchtextFromUserFrag);
//            requestForGetUsersTransaction("10", "1", myPrefs.getId(), "id", "desc", searchoptionFromUserFrag, searchtextFromUserFrag);
//        } else {
//            requestForGetUsersTransaction("10", "1", myPrefs.getId(), "id", "desc", "", "");
//        }
//    }
//
//    private void spinnerworks(View view) {
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spin.setAdapter(adapter);
//        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//    }
//
//    public void requestForGetUsersTransaction(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
//        try {
//            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
//            pojoRequestGetUsersData.setPer_page_record(per_page_record);
//            pojoRequestGetUsersData.setPage_no(page_no);
//            pojoRequestGetUsersData.setUser_id(user_id);
//            pojoRequestGetUsersData.setFilter(filter);
//            pojoRequestGetUsersData.setType(type);
//
//            pojoRequestGetUsersData.setSearch_option(searchOption);
//            pojoRequestGetUsersData.setSearch_text(searchText);
//            Gson gson = new Gson();
//            String jsonString = gson.toJson(pojoRequestGetUsersData);
//            Logger.logDebug("kamal", " requestForGetUsers jsonString : " + jsonString);
//            Logger.logDebug("kamal", " requestForGetUsers url : " + MyConstants.URL_CONTRACTOR_GETUSERSTRANSACTION);
//            JSONObject request = new JSONObject(jsonString);
//            if (request != null) {
//                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETUSERSTRANSACTION, request, new com.android.volley.Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        if (dialogLoading.isShowing()) {
//                            dialogLoading.dismiss();
//                        }
//                        try {
//                            if (response != null) {
//                                Logger.logDebug("kamal", " requestForGetUsers response : " + response.toString());
//                                Gson gson = new Gson();
//                                ResponseGetUsersTransact responseGetUsersTransact = gson.fromJson(response.toString(), ResponseGetUsersTransact.class);
//                                if (responseGetUsersTransact.getError().equalsIgnoreCase("false")) {
//
//                                    if (!responseGetUsersTransact.getContractors().getTotal_records().equals("0")) {
//
//                                        list.clear();
//                                        list.addAll(responseGetUsersTransact.getContractors().getUsertransactcontractors());
//                                        adapterUsersTransaction.notifyDataSetChanged();
//                                    } else {
//                                        list.clear();
//                                        adapterUsersTransaction.notifyDataSetChanged();
//                                        tvResponse.setVisibility(View.VISIBLE);
//                                        tvResponse.setText("No Data Found");
//                                    }
////
//                                } else {
//                                    Toast.makeText(getActivity(), responseGetUsersTransact.getMessage(), Toast.LENGTH_SHORT).show();
//                                }
//                            } else {
//                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Logger.logDebug("kamal", " requestForGetUsers error : " + error.getLocalizedMessage());
//                        if (dialogLoading.isShowing()) {
//                            dialogLoading.dismiss();
//                        }
//                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
//                    }
//                }) {
//
//                    /**
//                     * Passing some request headers
//                     * */
//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        HashMap<String, String> headers = new HashMap<String, String>();
//                        headers.put("Content-Type", "application/json");
//                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
//                        return headers;
//                    }
//
//                };
//
//                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETUSERSTRANSACTIONS);
//                dialogOpeningApp();
//            }
//
//        } catch (Exception e) {
//            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
//            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    private void dialogOpeningApp() {
//        dialogLoading = new Dialog(getActivity());
//        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialogLoading.setContentView(R.layout.dialog_loading_singin);
//        dialogLoading.setCancelable(true);
//        dialogLoading.setCanceledOnTouchOutside(false);
//        dialogLoading.show();
//        Window window = dialogLoading.getWindow();
//        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//    }
//
//
//}

