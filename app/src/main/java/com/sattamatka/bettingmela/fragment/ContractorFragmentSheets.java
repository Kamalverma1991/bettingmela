package com.sattamatka.bettingmela.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.activity.ContractorAddSheet;
import com.sattamatka.bettingmela.activity.ContractorEditSheet;
import com.sattamatka.bettingmela.activity.SheetsClickListener;
import com.sattamatka.bettingmela.adapters.AdapterSheets;
import com.sattamatka.bettingmela.model.request.PojoRequestGetSheetbyId;
import com.sattamatka.bettingmela.model.request.PojoRequestGetUsersData;
import com.sattamatka.bettingmela.model.response.ResponseGetUsers;
import com.sattamatka.bettingmela.model.response.ResponseSheetById;
import com.sattamatka.bettingmela.model.response.ResponseSheetInfoDetail;
import com.sattamatka.bettingmela.model.response.ResponseSheets;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_DELETESHEET;
import static com.sattamatka.bettingmela.utils.Tags.TAG_GETSHEETS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_GETUSERS;

public class ContractorFragmentSheets extends Fragment implements SheetsClickListener, View.OnClickListener {

    TextView headingId;
    TextView headingUserId;
    TextView headingContractorId;
    TextView headingTotalNoBet;
    TextView headingAmount;
    TextView headingTotalProfit;
    TextView headingWindowName;
    TextView headingWindowDate;
    TextView headingAddedDate;

    AlertDialog alertdialogAddSheet;
    Dialog dialogLoading;
    private MyPrefs myPrefs;
    private VolleyWrappers volleyWrappers = null;

    private List<ResponseSheetInfoDetail> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private AdapterSheets adapterSheets;
    String[] users = {"Select", "ID", "User ID", "Contractor ID", "Amount", "Window Name"};
    Spinner spin;
    private EditText etSearch;
    private ImageView ivSearch;
    private TextView tvResponse;
    private FloatingActionButton fab;


    String filter = "";
    String type = "";
    String searchoption = "";
    String searchtext = "";
    private int totalRecords = 0;
    public int pageNo = 1;
    boolean isLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contractor_fragment_sheets, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.out.println("ganeshajijijij oncreate :");
        myPrefs = new MyPrefs(getActivity());
        volleyWrappers = new VolleyWrappers(getActivity());

        headingId = view.findViewById(R.id.heading_id);
        headingUserId = view.findViewById(R.id.heading_userid);
        headingContractorId = view.findViewById(R.id.heading_contractorid);
        headingTotalNoBet = view.findViewById(R.id.heading_totalnobet);
        headingAmount = view.findViewById(R.id.heading_amount);
        headingTotalProfit = view.findViewById(R.id.heading_totalprofit);
        headingWindowName = view.findViewById(R.id.heading_windowname);
        headingWindowDate = view.findViewById(R.id.heading_windowdate);
        headingAddedDate = view.findViewById(R.id.heading_addeddate);

        headingId.setOnClickListener(this);
        headingId.setTag("ic_down");
        headingUserId.setOnClickListener(this);
        headingUserId.setTag("ic_down");
        headingContractorId.setOnClickListener(this);
        headingContractorId.setTag("ic_down");
        headingTotalNoBet.setOnClickListener(this);
        headingTotalNoBet.setTag("ic_down");
        headingAmount.setOnClickListener(this);
        headingAmount.setTag("ic_down");
        headingTotalProfit.setOnClickListener(this);
        headingTotalProfit.setTag("ic_down");
        headingWindowName.setOnClickListener(this);
        headingWindowName.setTag("ic_down");
        headingWindowDate.setOnClickListener(this);
        headingWindowDate.setTag("ic_down");
        headingAddedDate.setOnClickListener(this);
        headingAddedDate.setTag("ic_down");

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        spin = (Spinner) view.findViewById(R.id.spinner);
        etSearch = view.findViewById(R.id.edittext_search);
        ivSearch = view.findViewById(R.id.imageview_search);
        tvResponse = view.findViewById(R.id.textview_response);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAddUser();
            }
        });
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filter="id";
                type="desc";
                searchoption = spin.getSelectedItem().toString().replace(" ", "_").toLowerCase();
                searchtext = etSearch.getText().toString();
                pageNo = 1;
                if (searchoption.equals("Select")) {
                    Toast.makeText(getActivity(), "Please select filter before search", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(searchtext)) {
                    etSearch.setError("enter text");
                    Toast.makeText(getActivity(), "Please enter search text", Toast.LENGTH_SHORT).show();
                } else {
                    requestForGetSheets("10", String.valueOf(pageNo), myPrefs.getId(),  filter, type, searchoption, searchtext);

                }


            }
        });

        recyclerView = view.findViewById(R.id.recycler_view_sheets);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        adapterSheets = new AdapterSheets(list, this);
        recyclerView.setAdapter(adapterSheets);

        spinnerworks(view);
        initScrollListener();


    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("ganeshajijijij onResume :");
        resetData("id","desc");
    }

    private void spinnerworks(View view) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1 && list.size() < totalRecords) {
                        pageNo = pageNo + 1;
                        loadMore("10", String.valueOf(pageNo), myPrefs.getId(),  filter, type, searchoption, searchtext);
                        isLoading = true;
                    }
                }
            }
        });

    }

    public void resetData(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        spin.setSelection(0);
        etSearch.setText("");
        filter=fltr;
        type=typ;
        searchoption = "";
        searchtext = "";
        pageNo = 1;
        requestForGetSheets("10", String.valueOf(pageNo), myPrefs.getId(),  filter, type, searchoption, searchtext);

        headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingId.setTag("ic_down");
        headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingUserId.setTag("ic_down");
        headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingContractorId.setTag("ic_down");
        headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingTotalNoBet.setTag("ic_down");
        headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingAmount.setTag("ic_down");
        headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingTotalProfit.setTag("ic_down");
        headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingWindowName.setTag("ic_down");
        headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingWindowDate.setTag("ic_down");
        headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingAddedDate.setTag("ic_down");


    }

    public void ascDesc(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        filter=fltr;
        type=typ;
        pageNo = 1;
        requestForGetSheets("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
    }
    public void requestForGetSheets(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);

            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetSheets jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetSheets url : " + MyConstants.URL_CONTRACTOR_GETSHEETS);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETSHEETS, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetSheets response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseSheets responseSheets = gson.fromJson(response.toString(), ResponseSheets.class);
                                if (responseSheets.getError().equalsIgnoreCase("false")) {

                                    Logger.logDebug("kamal", " requestForGetUsers false : " + responseSheets.getError());
                                    if (!responseSheets.getSheets().getTotal_records().equals("0")) {


                                        totalRecords = Integer.parseInt(responseSheets.getSheets().getTotal_records());
                                        tvResponse.setVisibility(View.GONE);
                                        list.clear();
                                        list.addAll(responseSheets.getSheets().getSheet());
                                        adapterSheets.notifyDataSetChanged();
                                        tvResponse.setVisibility(View.GONE);
                                    } else {
                                        totalRecords = Integer.parseInt(responseSheets.getSheets().getTotal_records());
                                        list.clear();
                                        adapterSheets.notifyDataSetChanged();
                                        tvResponse.setVisibility(View.VISIBLE);
                                        tvResponse.setText("No Data Found");
                                    }


//                                    Logger.logDebug("kamal", " requestForGetSheets list : " + responseSheets.getSheets().getSheet().size());
//                                    list.addAll(responseSheets.getSheets().getSheet());
//                                    adapterSheets.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(getActivity(), responseSheets.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetSheets error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETSHEETS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }
    private void loadMore(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        list.add(null);
        adapterSheets.notifyItemInserted(list.size() - 1);
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);

            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetSheets jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetSheets url : " + MyConstants.URL_CONTRACTOR_GETSHEETS);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETSHEETS, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetSheets response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseSheets responseSheets = gson.fromJson(response.toString(), ResponseSheets.class);
                                if (responseSheets.getError().equalsIgnoreCase("false")) {

                                    Logger.logDebug("kamal", " requestForGetUsers false : " + responseSheets.getError());
                                    if (!responseSheets.getSheets().getTotal_records().equals("0")) {

                                        totalRecords = Integer.parseInt( responseSheets.getSheets().getTotal_records());
                                        list.remove(list.size() - 1);
                                        int scrollPosition = list.size();
                                        adapterSheets.notifyItemRemoved(scrollPosition);
                                        list.addAll(responseSheets.getSheets().getSheet());
                                        adapterSheets.notifyDataSetChanged();
                                        isLoading = false;
                                    } else {

                                    }


                                } else {
                                    Toast.makeText(getActivity(), responseSheets.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetSheets error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETSHEETS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }

    }
    private void dialogOpeningApp() {
        dialogLoading = new Dialog(getActivity());
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }


    @Override
    public void onSheetClicked(int position) {
        Intent editSheets = new Intent(getActivity(), ContractorEditSheet.class);
        editSheets.putExtra("user_id", list.get(position).getContractor_id());
        editSheets.putExtra("sheet_id", list.get(position).getId());
        getActivity().startActivity(editSheets);


    }

    @Override
    public void onSheetClickDelete(int position) {
        requestForDeleteSheet(myPrefs.getId(), list.get(position).getId());
    }

    private void requestForDeleteSheet(String user_id, String sheet_id) {
        try {
            PojoRequestGetSheetbyId pojoRequestGetSheetbyId = new PojoRequestGetSheetbyId();

            pojoRequestGetSheetbyId.setUser_id(user_id);
            pojoRequestGetSheetbyId.setSheet_id(sheet_id);

            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetSheetbyId);
            Logger.logDebug("kamal", " requestForDeleteSheet jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForDeleteSheet url : " + MyConstants.URL_CONTRACTOR_DELETESHEET);
            JSONObject request = new JSONObject(jsonString);
            Logger.logDebug("kamal", " requestForDeleteSheet request : " + jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_DELETESHEET, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForDeleteSheet response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseSheetById responseSheetById = gson.fromJson(response.toString(), ResponseSheetById.class);

                                if (responseSheetById.getError().equalsIgnoreCase("false")) {
                                    Toast.makeText(getActivity(), responseSheetById.getMessage(), Toast.LENGTH_SHORT).show();
                                    onResume();
                                } else {
                                    Toast.makeText(getActivity(), responseSheetById.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForDeleteSheet error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_DELETESHEET);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogAddUser() {
        final Calendar myCalendar = Calendar.getInstance();
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.dialog_addsheet, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);


        String[] strWindowName = {"Select", "JACKPOT", "NEW FARIDABAAD", "GAZIABAAD", "GALI", "DISAWAR"};
        final ImageView ivCancel = (ImageView) promptsView.findViewById(R.id.imageview_cancel);
        final Spinner spinnerWindowName = (Spinner) promptsView.findViewById(R.id.spinner);
        ArrayAdapter<String> adapterWindowName = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, strWindowName);
        adapterWindowName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWindowName.setAdapter(adapterWindowName);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialogAddSheet.dismiss();
            }
        });
        final EditText etDate = (EditText) promptsView.findViewById(R.id.edittext_date);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                etDate.setText(sdf.format(myCalendar.getTime()));
            }
        };
        etDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final TextView tvSubmit = (TextView) promptsView.findViewById(R.id.textview_submit);
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String windowName = spinnerWindowName.getSelectedItem().toString().toLowerCase().trim();
                String windowId = "0";
                if (windowName.equals("select")) {
                    Toast.makeText(getActivity(), "Please select window name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etDate.getText().toString())) {
                    etDate.setError("Please enter date");
                    Toast.makeText(getActivity(), "Please enter date", Toast.LENGTH_SHORT).show();
                } else {

                    if (windowName.equalsIgnoreCase("jackpot")) {
                        windowId = "1";
                    } else if (windowName.equalsIgnoreCase("new faridabaad")) {
                        windowId = "2";
                    } else if (windowName.equalsIgnoreCase("gaziabaad")) {
                        windowId = "3";
                    } else if (windowName.equalsIgnoreCase("gali")) {
                        windowId = "4";
                    } else if (windowName.equalsIgnoreCase("disawar")) {
                        windowId = "5";
                    }

                    Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate sending trueornot : " + windowName.equalsIgnoreCase("gaziabaad"));
                    Logger.logDebug("kamal", " requestForGetSheetAddOrUpdate sending windowname : " + windowName + "  windowid: " + windowId);
                    Intent addSheet = new Intent(getActivity(), ContractorAddSheet.class);
                    addSheet.putExtra("user_id", myPrefs.getId());
                    addSheet.putExtra("sheet_id", "0");
                    addSheet.putExtra("window_name", windowName);
                    addSheet.putExtra("window_id", windowId);
                    addSheet.putExtra("booking_date", etDate.getText().toString());
                    getActivity().startActivity(addSheet);
                    alertdialogAddSheet.dismiss();
                }
            }
        });


        alertDialogBuilder.setCancelable(true);
        alertdialogAddSheet = alertDialogBuilder.create();
        alertdialogAddSheet.show();
    }

    @Override
    public void onClick(View view) {
        if(view==headingId){
            if(headingId.getTag().equals("ic_down")){
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingId.setTag("ic_up");
                ascDesc("id","asc");
            }else{
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingId.setTag("ic_down");
                ascDesc("id","desc");
            }
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingUserId){
            if(headingUserId.getTag().equals("ic_down")){
                headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingUserId.setTag("ic_up");
                ascDesc("user_id","asc");
            }else{
                headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingUserId.setTag("ic_down");
                ascDesc("user_id","desc");
            }

            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingContractorId){
            if(headingContractorId.getTag().equals("ic_down")){
                headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingContractorId.setTag("ic_up");
                ascDesc("contractor_id","asc");
            }else{
                headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingContractorId.setTag("ic_down");
                ascDesc("contractor_id","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");



        }
        if(view==headingTotalNoBet){
            if(headingTotalNoBet.getTag().equals("ic_down")){
                headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingTotalNoBet.setTag("ic_up");
                ascDesc("total_no_bet","asc");
            }else{
                headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingTotalNoBet.setTag("ic_down");
                ascDesc("total_no_bet","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingAmount){
            if(headingAmount.getTag().equals("ic_down")){
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAmount.setTag("ic_up");
                ascDesc("amount","asc");
            }else{
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAmount.setTag("ic_down");
                ascDesc("amount","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingTotalProfit){
            if(headingTotalProfit.getTag().equals("ic_down")){
                headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingTotalProfit.setTag("ic_up");
                ascDesc("total_profit","asc");
            }else{
                headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingTotalProfit.setTag("ic_down");
                ascDesc("total_profit","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingWindowName){
            if(headingWindowName.getTag().equals("ic_down")){
                headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingWindowName.setTag("ic_up");
                ascDesc("window_name","asc");
            }else{
                headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingWindowName.setTag("ic_down");
                ascDesc("window_name","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingWindowDate){
            if(headingWindowDate.getTag().equals("ic_down")){
                headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingWindowDate.setTag("ic_up");
                ascDesc("window_date","asc");
            }else{
                headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingWindowDate.setTag("ic_down");
                ascDesc("window_date","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedDate.setTag("ic_down");


        }
        if(view==headingAddedDate){
            if(headingAddedDate.getTag().equals("ic_down")){
                headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAddedDate.setTag("ic_up");
                ascDesc("added_on","asc");
            }else{
                headingAddedDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAddedDate.setTag("ic_down");
                ascDesc("added_on","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingTotalNoBet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalNoBet.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalProfit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalProfit.setTag("ic_down");
            headingWindowName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowName.setTag("ic_down");
            headingWindowDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingWindowDate.setTag("ic_down");


        }
    }
}
