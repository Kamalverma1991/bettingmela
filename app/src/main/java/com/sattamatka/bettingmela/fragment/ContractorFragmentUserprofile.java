package com.sattamatka.bettingmela.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.adapters.AdapterMyTransactions;
import com.sattamatka.bettingmela.model.request.PojoRequestGetUsersData;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransact;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransactInfoContractors;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_GETMYTRANSACTIONS;

public class ContractorFragmentUserprofile extends Fragment implements View.OnClickListener {


    TextView headingId;
    TextView headingContractorId;
    TextView headingBettingId;
    TextView headingAmount;
    TextView headingTotalAmount;
    TextView headingType;
    TextView headingAddedon;

    private NestedScrollView nestedScrollView;
    private EditText etUsername;
    private EditText etEmail;
    private EditText etMobile;
    private EditText etPassword;
    private TextView tvResponse;

    Dialog dialogLoading;
    private MyPrefs myPrefs;
    private VolleyWrappers volleyWrappers = null;

    private List<ResponseGetUsersTransactInfoContractors> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private AdapterMyTransactions adapterMyTransactions;


    String filter = "";
    String type = "";
    String searchoption = "";
    String searchtext = "";
    private int totalRecords = 0;
    public int pageNo = 1;
    boolean isLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contractor_fragment_userprofile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myPrefs = new MyPrefs(getActivity());
        volleyWrappers = new VolleyWrappers(getActivity());

        headingId = view.findViewById(R.id.heading_id);
        headingContractorId = view.findViewById(R.id.heading_contractorid);
        headingBettingId = view.findViewById(R.id.heading_bettingid);
        headingAmount = view.findViewById(R.id.heading_amount);
        headingTotalAmount = view.findViewById(R.id.heading_totalamount);
        headingType = view.findViewById(R.id.heading_type);
        headingAddedon = view.findViewById(R.id.heading_addedon);

        headingId.setOnClickListener(this);
        headingId.setTag("ic_down");
        headingContractorId.setOnClickListener(this);
        headingContractorId.setTag("ic_down");
        headingBettingId.setOnClickListener(this);
        headingBettingId.setTag("ic_down");
        headingAmount.setOnClickListener(this);
        headingAmount.setTag("ic_down");
        headingTotalAmount.setOnClickListener(this);
        headingTotalAmount.setTag("ic_down");
        headingType.setOnClickListener(this);
        headingType.setTag("ic_down");
        headingAddedon.setOnClickListener(this);
        headingAddedon.setTag("ic_down");


        nestedScrollView = view.findViewById(R.id.newstedscrollview);
        etUsername = view.findViewById(R.id.edittext_username);
        etEmail = view.findViewById(R.id.edittext_email);
        etMobile = view.findViewById(R.id.edittext_mobile);
        tvResponse = view.findViewById(R.id.textview_response);
        etUsername.setText(new MyPrefs(getActivity()).getUsername());
        etEmail.setText(new MyPrefs(getActivity()).getEmail());
        etMobile.setText(new MyPrefs(getActivity()).getMobile());

        recyclerView = view.findViewById(R.id.recycler_view_usertransaction);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        adapterMyTransactions = new AdapterMyTransactions(list);
        recyclerView.setAdapter(adapterMyTransactions);
        filter="id";
        type="desc";
        searchoption = "";
        searchtext = "";
        pageNo = 1;
        requestForGetMyTransactions("20", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
        initScrollListener();
    }

    private void initScrollListener() {
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//                if (!isLoading) {
//                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1 && list.size() < totalRecords) {
//                        pageNo = pageNo + 1;
//                        loadMore("10", String.valueOf(pageNo), myPrefs.getId(), "id", "desc", searchoption, searchtext);
//                        isLoading = true;
//                    }
//                }
//            }
//        });


        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        if (!isLoading) {
                            if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1 && list.size() < totalRecords) {
                                pageNo = pageNo + 1;
                                loadMore("20", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
                                isLoading = true;
                            }
                        }




                    }
                }
            }
        });
    }

    public void ascDesc(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        filter=fltr;
        type=typ;
        pageNo = 1;
        requestForGetMyTransactions("20", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
    }
    private void requestForGetMyTransactions(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);
            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetMyTransactions jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetMyTransactions url : " + MyConstants.URL_CONTRACTOR_GETMYTRANSACTION);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETMYTRANSACTION, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetMyTransactions response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetUsersTransact responseGetUsersTransact = gson.fromJson(response.toString(), ResponseGetUsersTransact.class);
                                if (responseGetUsersTransact.getError().equalsIgnoreCase("false")) {

                                    if (!responseGetUsersTransact.getContractors().getTotal_records().equals("0")) {

                                        totalRecords = Integer.parseInt(responseGetUsersTransact.getContractors().getTotal_records());
                                        tvResponse.setVisibility(View.GONE);
                                        list.clear();
                                        list.addAll(responseGetUsersTransact.getContractors().getUsertransactcontractors());
                                        System.out.println("Heyganeshnesteddd insideisloadingfalse0:  "+(list.size()));
                                        adapterMyTransactions.notifyDataSetChanged();
                                    } else {

                                        totalRecords = Integer.parseInt(responseGetUsersTransact.getContractors().getTotal_records());
                                        list.clear();
                                        adapterMyTransactions.notifyDataSetChanged();
                                        tvResponse.setVisibility(View.VISIBLE);
                                        tvResponse.setText("No Data Found");
                                    }
//
                                } else {
                                    Toast.makeText(getActivity(), responseGetUsersTransact.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetMyTransactions error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETMYTRANSACTIONS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void loadMore(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        list.add(null);
        adapterMyTransactions.notifyItemInserted(list.size() - 1);
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);
            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " loadMore jsonString : " + jsonString);
            Logger.logDebug("kamal", " loadMore url : " + MyConstants.URL_CONTRACTOR_GETMYTRANSACTION);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETMYTRANSACTION, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " loadMore response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetUsersTransact responseGetUsersTransact = gson.fromJson(response.toString(), ResponseGetUsersTransact.class);
                                if (responseGetUsersTransact.getError().equalsIgnoreCase("false")) {

                                    if (!responseGetUsersTransact.getContractors().getTotal_records().equals("0")) {

                                        totalRecords = Integer.parseInt(responseGetUsersTransact.getContractors().getTotal_records());
                                        list.remove(list.size() - 1);
                                        int scrollPosition = list.size();
                                        adapterMyTransactions.notifyItemRemoved(scrollPosition);
                                        list.addAll(responseGetUsersTransact.getContractors().getUsertransactcontractors());
                                        adapterMyTransactions.notifyDataSetChanged();
                                        isLoading = false;


                                    } else {

                                    }
//
                                } else {
                                    Toast.makeText(getActivity(), responseGetUsersTransact.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " loadMore error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETMYTRANSACTIONS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogOpeningApp() {
        dialogLoading = new Dialog(getActivity());
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }


    @Override
    public void onClick(View view) {
        if(view==headingId){
            if(headingId.getTag().equals("ic_down")){
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingId.setTag("ic_up");
                ascDesc("id","asc");
            }else{
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingId.setTag("ic_down");
                ascDesc("id","desc");
            }
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");

        }
        if(view==headingContractorId){
            if(headingContractorId.getTag().equals("ic_down")){
                headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingContractorId.setTag("ic_up");
                ascDesc("user_id","asc");
            }else{
                headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingContractorId.setTag("ic_down");
                ascDesc("user_id","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingBettingId){
            if(headingBettingId.getTag().equals("ic_down")){
                headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingBettingId.setTag("ic_up");
                ascDesc("betting_id","asc");
            }else{
                headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingBettingId.setTag("ic_down");
                ascDesc("betting_id","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");

        }
        if(view==headingAmount){
            if(headingAmount.getTag().equals("ic_down")){
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAmount.setTag("ic_up");
                ascDesc("amount","asc");
            }else{
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAmount.setTag("ic_down");
                ascDesc("amount","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingTotalAmount){
            if(headingTotalAmount.getTag().equals("ic_down")){
                headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingTotalAmount.setTag("ic_up");
                ascDesc("total_amount","asc");
            }else{
                headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingTotalAmount.setTag("ic_down");
                ascDesc("total_amount","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingType){
            if(headingType.getTag().equals("ic_down")){
                headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingType.setTag("ic_up");
                ascDesc("type","asc");
            }else{
                headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingType.setTag("ic_down");
                ascDesc("type","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingAddedon){
            if(headingAddedon.getTag().equals("ic_down")){
                headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAddedon.setTag("ic_up");
                ascDesc("added_on","asc");
            }else{
                headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAddedon.setTag("ic_down");
                ascDesc("added_on","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingContractorId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingContractorId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
        }
    }
}
