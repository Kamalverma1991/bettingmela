package com.sattamatka.bettingmela.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.adapters.AdapterUsersTransaction;
import com.sattamatka.bettingmela.model.request.PojoRequestGetUsersData;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransact;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersTransactInfoContractors;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_GETUSERSTRANSACTIONS;

public class ContractorFragmentUserstransaction extends Fragment implements View.OnClickListener {

    TextView headingId;
    TextView headingUserId;
    TextView headingBettingId;
    TextView headingAmount;
    TextView headingTotalAmount;
    TextView headingType;
    TextView headingAddedon;


    Dialog dialogLoading;
    private MyPrefs myPrefs;
    private VolleyWrappers volleyWrappers = null;

    private List<ResponseGetUsersTransactInfoContractors> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private AdapterUsersTransaction adapterUsersTransaction;
    String[] users = {"Select", "ID", "User ID", "Betting ID", "Amount", "Type"};
    Spinner spin;
    private EditText etSearch;
    private ImageView ivSearch;
    private TextView tvResponse;
    String filter = "";
    String type = "";
    String searchoption = "";
    String searchtext = "";
    private int totalRecords = 0;
    public int pageNo = 1;
    boolean isLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contractor_fragment_usertransaction, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myPrefs = new MyPrefs(getActivity());
        volleyWrappers = new VolleyWrappers(getActivity());

        headingId = view.findViewById(R.id.heading_id);
        headingUserId = view.findViewById(R.id.heading_userid);
        headingBettingId = view.findViewById(R.id.heading_bettingid);
        headingAmount = view.findViewById(R.id.heading_amount);
        headingTotalAmount = view.findViewById(R.id.heading_totalamount);
        headingType = view.findViewById(R.id.heading_type);
        headingAddedon = view.findViewById(R.id.heading_addedon);

        headingId.setOnClickListener(this);
        headingId.setTag("ic_down");
        headingUserId.setOnClickListener(this);
        headingUserId.setTag("ic_down");
        headingBettingId.setOnClickListener(this);
        headingBettingId.setTag("ic_down");
        headingAmount.setOnClickListener(this);
        headingAmount.setTag("ic_down");
        headingTotalAmount.setOnClickListener(this);
        headingTotalAmount.setTag("ic_down");
        headingType.setOnClickListener(this);
        headingType.setTag("ic_down");
        headingAddedon.setOnClickListener(this);
        headingAddedon.setTag("ic_down");


        spin = (Spinner) view.findViewById(R.id.spinner);
        etSearch = view.findViewById(R.id.edittext_search);
        ivSearch = view.findViewById(R.id.imageview_search);
        tvResponse = view.findViewById(R.id.textview_response);
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter="id";
                type="desc";
                searchoption = spin.getSelectedItem().toString().replace(" ", "_").toLowerCase();
                searchtext = etSearch.getText().toString();
                pageNo = 1;
                if (searchoption.equals("Select")) {
                    Toast.makeText(getActivity(), "Please select filter before search", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(searchtext)) {
                    etSearch.setError("enter text");
                    Toast.makeText(getActivity(), "Please enter search text", Toast.LENGTH_SHORT).show();
                } else {
                    requestForGetUsersTransaction("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);

                }


            }
        });

        recyclerView = view.findViewById(R.id.recycler_view_usertransaction);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        adapterUsersTransaction = new AdapterUsersTransaction(list);
        recyclerView.setAdapter(adapterUsersTransaction);
        spinnerworks(view);
        if (getArguments() != null) {
            filter="id";
            type="desc";
            searchoption = getArguments().getString("searchoption");
            searchtext = getArguments().getString("searchtext");
            pageNo = 1;
            spin.setSelection(2);
            etSearch.setText(searchtext);
            requestForGetUsersTransaction("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
        } else {
            filter="id";
            type="desc";
            searchoption = "";
            searchtext = "";
            pageNo = 1;
            requestForGetUsersTransaction("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
        }
        initScrollListener();
    }

    private void spinnerworks(View view) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1 && list.size() < totalRecords) {
                        pageNo = pageNo + 1;
                        loadMore("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
                        isLoading = true;
                    }
                }
            }
        });


    }

    public void resetData(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        spin.setSelection(0);
        etSearch.setText("");
        filter=fltr;
        type=typ;
        searchoption = "";
        searchtext = "";
        pageNo = 1;
        requestForGetUsersTransaction("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);

        headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingId.setTag("ic_down");
        headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingUserId.setTag("ic_down");
        headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingBettingId.setTag("ic_down");
        headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingAmount.setTag("ic_down");
        headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingTotalAmount.setTag("ic_down");
        headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingType.setTag("ic_down");
        headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingAddedon.setTag("ic_down");
    }
    public void ascDesc(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        filter=fltr;
        type=typ;
        pageNo = 1;
        requestForGetUsersTransaction("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);
    }

    public void requestForGetUsersTransaction(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);
            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetUsersTransaction jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetUsersTransaction url : " + MyConstants.URL_CONTRACTOR_GETUSERSTRANSACTION);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETUSERSTRANSACTION, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetUsersTransaction response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetUsersTransact responseGetUsersTransact = gson.fromJson(response.toString(), ResponseGetUsersTransact.class);
                                if (responseGetUsersTransact.getError().equalsIgnoreCase("false")) {

                                    if (!responseGetUsersTransact.getContractors().getTotal_records().equals("0")) {
                                        totalRecords = Integer.parseInt(responseGetUsersTransact.getContractors().getTotal_records());
                                        tvResponse.setVisibility(View.GONE);
                                        list.clear();
                                        list.addAll(responseGetUsersTransact.getContractors().getUsertransactcontractors());
                                        adapterUsersTransaction.notifyDataSetChanged();
                                    } else {
                                        totalRecords = Integer.parseInt(responseGetUsersTransact.getContractors().getTotal_records());
                                        list.clear();
                                        adapterUsersTransaction.notifyDataSetChanged();
                                        tvResponse.setVisibility(View.VISIBLE);
                                        tvResponse.setText("No Data Found");
                                    }
//
                                } else {
                                    Toast.makeText(getActivity(), responseGetUsersTransact.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetUsersTransaction error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETUSERSTRANSACTIONS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogOpeningApp() {
        dialogLoading = new Dialog(getActivity());
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void loadMore(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        list.add(null);
        adapterUsersTransaction.notifyItemInserted(list.size() - 1);
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);
            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetUsers jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetUsers url : " + MyConstants.URL_CONTRACTOR_GETUSERSTRANSACTION);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETUSERSTRANSACTION, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetUsers response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetUsersTransact responseGetUsersTransact = gson.fromJson(response.toString(), ResponseGetUsersTransact.class);
                                if (responseGetUsersTransact.getError().equalsIgnoreCase("false")) {

                                    if (!responseGetUsersTransact.getContractors().getTotal_records().equals("0")) {
                                        totalRecords = Integer.parseInt(responseGetUsersTransact.getContractors().getTotal_records());
                                        list.remove(list.size() - 1);
                                        int scrollPosition = list.size();
                                        adapterUsersTransaction.notifyItemRemoved(scrollPosition);
                                        list.addAll(responseGetUsersTransact.getContractors().getUsertransactcontractors());

                                        adapterUsersTransaction.notifyDataSetChanged();
                                        isLoading = false;


//                                        list.clear();
//                                        list.addAll(responseGetUsersTransact.getContractors().getUsertransactcontractors());
//                                        adapterUsersTransaction.notifyDataSetChanged();
                                    } else {
//                                        list.clear();
//                                        adapterUsersTransaction.notifyDataSetChanged();
//                                        tvResponse.setVisibility(View.VISIBLE);
//                                        tvResponse.setText("No Data Found");
                                    }
//
                                } else {
                                    Toast.makeText(getActivity(), responseGetUsersTransact.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetUsers error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETUSERSTRANSACTIONS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        if(view==headingId){
            if(headingId.getTag().equals("ic_down")){
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingId.setTag("ic_up");
                ascDesc("id","asc");
            }else{
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingId.setTag("ic_down");
                ascDesc("id","desc");
            }
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");

        }
        if(view==headingUserId){
            if(headingUserId.getTag().equals("ic_down")){
                headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingUserId.setTag("ic_up");
                ascDesc("user_id","asc");
            }else{
                headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingUserId.setTag("ic_down");
                ascDesc("user_id","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingBettingId){
            if(headingBettingId.getTag().equals("ic_down")){
                headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingBettingId.setTag("ic_up");
                ascDesc("betting_id","asc");
            }else{
                headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingBettingId.setTag("ic_down");
                ascDesc("betting_id","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");

        }
        if(view==headingAmount){
            if(headingAmount.getTag().equals("ic_down")){
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAmount.setTag("ic_up");
                ascDesc("amount","asc");
            }else{
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAmount.setTag("ic_down");
                ascDesc("amount","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingTotalAmount){
            if(headingTotalAmount.getTag().equals("ic_down")){
                headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingTotalAmount.setTag("ic_up");
                ascDesc("total_amount","asc");
            }else{
                headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingTotalAmount.setTag("ic_down");
                ascDesc("total_amount","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingType){
            if(headingType.getTag().equals("ic_down")){
                headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingType.setTag("ic_up");
                ascDesc("type","asc");
            }else{
                headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingType.setTag("ic_down");
                ascDesc("type","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAddedon.setTag("ic_down");
        }
        if(view==headingAddedon){
            if(headingAddedon.getTag().equals("ic_down")){
                headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAddedon.setTag("ic_up");
                ascDesc("added_on","asc");
            }else{
                headingAddedon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAddedon.setTag("ic_down");
                ascDesc("added_on","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUserId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUserId.setTag("ic_down");
            headingBettingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingBettingId.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
            headingTotalAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingTotalAmount.setTag("ic_down");
            headingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingType.setTag("ic_down");
        }


    }
}
