package com.sattamatka.bettingmela.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.adapters.AdapterSpinner;
import com.sattamatka.bettingmela.model.request.PojoRequestGetDashboardData;
import com.sattamatka.bettingmela.model.response.ResponseGetDashboarddata;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;
import com.sattamatka.bettingmela.utils.MySpinner;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_GETUSERSTRANSACTIONS;


public class ContractorFragmentDashboard extends Fragment {


    String[] dateList = {"Today", "Yesterday", "Last 7 Days", "Last 30 Days", "This Month", "Last Month", "Custom"};
    MySpinner spin;

    String range1 = "";
    String range2 = "";
    String firstDateSelected = "";
    String seconDateSelected = "";

    private TextView tvDateRange;
    private TextView tvUsers_total;
    private TextView tvUsers_totalUsers;
    private TextView tvUsers_disabledUsers;
    private TextView tvUsers_enabledUsers;


    private TextView tvSheets_totalSubmittedSheets;
    private TextView tvSheets_sheetsSubmitted;
    private TextView tvSheets_totalNoBet;
    private TextView tvSheets_totalProfit;
    private TextView tvSheets_totalLoss;

    Dialog dialogLoading;
    private MyPrefs myPrefs;
    private VolleyWrappers volleyWrappers = null;

    AlertDialog alertdialogDateRangeCustom;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contractor_fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myPrefs = new MyPrefs(getActivity());
        volleyWrappers = new VolleyWrappers(getActivity());
        tvDateRange = view.findViewById(R.id.texview_daterange);
        tvUsers_total = view.findViewById(R.id.textview_users_total);
        tvUsers_totalUsers = view.findViewById(R.id.textview_users_totalusers);
        tvUsers_disabledUsers = view.findViewById(R.id.textview_users_disabledusers);
        tvUsers_enabledUsers = view.findViewById(R.id.textview_users_enabledusers);

        tvSheets_totalSubmittedSheets = view.findViewById(R.id.textview_sheets_totalsubmittedsheets);
        tvSheets_sheetsSubmitted = view.findViewById(R.id.textview_sheets_sheetssubmitted);
        tvSheets_totalNoBet = view.findViewById(R.id.textview_sheets_totalnobet);
        tvSheets_totalProfit = view.findViewById(R.id.textview_sheets_totalprofit);
        tvSheets_totalLoss = view.findViewById(R.id.textview_sheets_totalloss);

        spinnerworks(view);
//        Date todayDate = new Date();
//        range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
//        range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
//        tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
//        requestForGetDashboardData(myPrefs.getId(), range1, range2);
    }

    private void spinnerworks(View view) {
        spin = (MySpinner) view.findViewById(R.id.spinner);
        AdapterSpinner adapter = new AdapterSpinner(getActivity(), R.layout.ghosttext, dateList, spin);
        spin.setAdapter(adapter);
        spin.setSelection(3);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                System.out.println("ganeshajijij position " + position);
                if (position == 0) {
                    Date todayDate = new Date();
                    range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
                    range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                } else if (position == 1) {
                    Date todayDate = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(todayDate);
                    calendar.add(Calendar.DAY_OF_YEAR, -1);
                    Date oldDate = calendar.getTime();
                    range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(oldDate);
                    range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                } else if (position == 2) {
                    Date todayDate = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(todayDate);
                    calendar.add(Calendar.DAY_OF_YEAR, -7);
                    Date oldDate = calendar.getTime();
                    range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(oldDate);
                    range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                } else if (position == 3) {
                    Date todayDate = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(todayDate);
                    calendar.add(Calendar.DAY_OF_YEAR, -30);
                    Date oldDate = calendar.getTime();
                    range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(oldDate);
                    range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(todayDate);
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                } else if (position == 4) {
                    //this month/
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MONTH, 0);
                    calendar.set(Calendar.DATE, 1);
                    Date oldDate = calendar.getTime();
                    range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(oldDate);

                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(calendar.getTime());
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                } else if (position == 5) {
                    //last month/
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MONTH, -1);
                    calendar.set(Calendar.DATE, 1);
                    Date oldDate = calendar.getTime();
                    range1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(oldDate);

                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    range2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(calendar.getTime());
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                } else if (position == 6) {
                    dialogCustomDateRange();
                }
//
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String dateConvertor(String actualDate) {
        String convertedValue = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(actualDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat month_date = new SimpleDateFormat("MMM dd ,yyyy", Locale.ENGLISH);
        convertedValue = month_date.format(date);
        return convertedValue;


    }

    public void requestForGetDashboardData(String user_id, String start_date, String end_date) {
        try {
            PojoRequestGetDashboardData pojoRequestGetDashboardData = new PojoRequestGetDashboardData();
            pojoRequestGetDashboardData.setUser_id(user_id);
            pojoRequestGetDashboardData.setStart_date(start_date);
            pojoRequestGetDashboardData.setEnd_date(end_date);

            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetDashboardData);
            Logger.logDebug("kamal", " requestForGetDashboardData jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetDashboardData url : " + MyConstants.URL_CONTRACTOR_GETDASHBOARDDATA);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETDASHBOARDDATA, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetDashboardData response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetDashboarddata responseGetDashboarddata = gson.fromJson(response.toString(), ResponseGetDashboarddata.class);
                                if (responseGetDashboarddata.getError().equalsIgnoreCase("false")) {

                                    tvUsers_total.setText(responseGetDashboarddata.getDashbboard().getUser().getTotal());
                                    tvUsers_totalUsers.setText(responseGetDashboarddata.getDashbboard().getUser().getTotal_users());
                                    tvUsers_disabledUsers.setText(responseGetDashboarddata.getDashbboard().getUser().getTotal_disabled());
                                    tvUsers_enabledUsers.setText(responseGetDashboarddata.getDashbboard().getUser().getTotal_enabled());

                                    tvSheets_totalSubmittedSheets.setText(responseGetDashboarddata.getDashbboard().getSheets().getTotal());
                                    tvSheets_sheetsSubmitted.setText(responseGetDashboarddata.getDashbboard().getSheets().getTotal_sheets());
                                    tvSheets_totalNoBet.setText(responseGetDashboarddata.getDashbboard().getSheets().getTotal_no_bet());
                                    tvSheets_totalProfit.setText(responseGetDashboarddata.getDashbboard().getSheets().getTotal_profit());
                                    tvSheets_totalLoss.setText(responseGetDashboarddata.getDashbboard().getSheets().getTotal_loss());

                                } else {
                                    Toast.makeText(getActivity(), responseGetDashboarddata.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetDashboardData error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETUSERSTRANSACTIONS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogOpeningApp() {
        dialogLoading = new Dialog(getActivity());
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void dialogCustomDateRange() {

        firstDateSelected = "";
        seconDateSelected = "";

        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.dialog_customdaterange, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);


        final DateRangeCalendarView calendarView = promptsView.findViewById(R.id.calendar);
        final TextView clear = (TextView) promptsView.findViewById(R.id.clear);
        final TextView submit = (TextView) promptsView.findViewById(R.id.submit);


        calendarView.setCalendarListener(new DateRangeCalendarView.CalendarListener() {
            @Override
            public void onFirstDateSelected(Calendar startDate) {
                System.out.println("ganeshaji   onFirstDateSelected: " + startDate.getTime());
            }

            @Override
            public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
                firstDateSelected = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(startDate.getTime());
                seconDateSelected = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(endDate.getTime());

            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialogDateRangeCustom.dismiss();
                firstDateSelected = "";
                seconDateSelected = "";
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialogDateRangeCustom.dismiss();

                if (!firstDateSelected.equals("") && !seconDateSelected.equals("")) {
                    range1 = firstDateSelected;
                    range2 = seconDateSelected;
                    tvDateRange.setText(dateConvertor(range1) + "  -  " + dateConvertor(range2));
                    requestForGetDashboardData(myPrefs.getId(), range1, range2);
                }
            }
        });

        alertDialogBuilder.setCancelable(false);
        alertdialogDateRangeCustom = alertDialogBuilder.create();
        alertdialogDateRangeCustom.show();
    }


    public class MyAdapter extends ArrayAdapter<String> {

        public MyAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            LayoutInflater inflater = getLayoutInflater();
            View spinnerItem = inflater.inflate(android.R.layout.simple_spinner_item, null);

            TextView mytext = (TextView) spinnerItem.findViewById(android.R.id.text1);
            mytext.setText(dateList[position]);

            //int selected = Spinner.
            int selected = spin.getSelectedItemPosition();
            if (position == selected) {
                spinnerItem.setBackgroundColor(Color.BLUE);
            }
            return spinnerItem;

        }

    }
}
