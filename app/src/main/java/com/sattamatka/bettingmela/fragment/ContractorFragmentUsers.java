package com.sattamatka.bettingmela.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.sattamatka.bettingmela.R;
import com.sattamatka.bettingmela.VolleyWrappers;
import com.sattamatka.bettingmela.activity.UsersListClickListeners;
import com.sattamatka.bettingmela.adapters.AdapterUsers;
import com.sattamatka.bettingmela.model.PojoRequestAddUserMoney;
import com.sattamatka.bettingmela.model.request.PojoRequestAddUser;
import com.sattamatka.bettingmela.model.request.PojoRequestDebitMoneyOtp;
import com.sattamatka.bettingmela.model.request.PojoRequestGetUsersData;
import com.sattamatka.bettingmela.model.response.ResponseAddUser;
import com.sattamatka.bettingmela.model.response.ResponseGetUsers;
import com.sattamatka.bettingmela.model.response.ResponseGetUsersInfoContractors;
import com.sattamatka.bettingmela.utils.Logger;
import com.sattamatka.bettingmela.utils.MyConstants;
import com.sattamatka.bettingmela.utils.MyPrefs;
import com.sattamatka.bettingmela.utils.UsefullUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sattamatka.bettingmela.utils.MyConstants.MES_OOPS;
import static com.sattamatka.bettingmela.utils.Tags.TAG_ADDUSER;
import static com.sattamatka.bettingmela.utils.Tags.TAG_ADDUSERMONEY;
import static com.sattamatka.bettingmela.utils.Tags.TAG_DEBITMONEYOTP;
import static com.sattamatka.bettingmela.utils.Tags.TAG_EDITUSER;
import static com.sattamatka.bettingmela.utils.Tags.TAG_GETUSERS;


public class ContractorFragmentUsers extends Fragment implements UsersListClickListeners, View.OnClickListener {


    TextView headingId;
    TextView headingUsername;
    TextView headingEmail;
    TextView headingMobile;
    TextView headingAmount;

    AlertDialog alertDialogUserCreditDebitAdd;
    Dialog dialogLoading;
    AlertDialog alertDialogEditUser;
    AlertDialog alertDialogAddUser;
    private MyPrefs myPrefs;
    private VolleyWrappers volleyWrappers = null;

    private List<ResponseGetUsersInfoContractors> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private AdapterUsers adapterUsers;
    String[] users = {"Select", "ID", "Username", "E-Mail", "Mobile"};
    Spinner spin;
    private EditText etSearch;
    private ImageView ivSearch;
    private TextView tvResponse;
    private FloatingActionButton fab;


    String filter = "";
    String type = "";
    String searchoption = "";
    String searchtext = "";
    private int totalRecords = 0;
    public int pageNo = 1;
    boolean isLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contractor_fragment_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myPrefs = new MyPrefs(getActivity());
        volleyWrappers = new VolleyWrappers(getActivity());


        headingId = view.findViewById(R.id.heading_id);
        headingUsername = view.findViewById(R.id.heading_username);
        headingEmail = view.findViewById(R.id.heading_email);
        headingMobile = view.findViewById(R.id.heading_mobile);
        headingAmount = view.findViewById(R.id.heading_amount);

        headingId.setOnClickListener(this);
        headingId.setTag("ic_down");
        headingUsername.setOnClickListener(this);
        headingUsername.setTag("ic_down");
        headingEmail.setOnClickListener(this);
        headingEmail.setTag("ic_down");
        headingMobile.setOnClickListener(this);
        headingMobile.setTag("ic_down");
        headingAmount.setOnClickListener(this);
        headingAmount.setTag("ic_down");


        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        spin = (Spinner) view.findViewById(R.id.spinner);
        etSearch = view.findViewById(R.id.edittext_search);
        ivSearch = view.findViewById(R.id.imageview_search);
        tvResponse = view.findViewById(R.id.textview_response);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAddUser();
            }
        });
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter="id";
                type="desc";
                searchoption = spin.getSelectedItem().toString();
                searchtext = etSearch.getText().toString();
                pageNo = 1;
                if (searchoption.equals("Select")) {
                    Toast.makeText(getActivity(), "Please select filter before search", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(searchtext)) {
                    etSearch.setError("enter text");
                    Toast.makeText(getActivity(), "Please enter search text", Toast.LENGTH_SHORT).show();
                } else {
                    requestForGetUsers("10", String.valueOf(pageNo), myPrefs.getId(), filter, type, searchoption, searchtext);

                }


            }
        });

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        adapterUsers = new AdapterUsers(list, this);
        recyclerView.setAdapter(adapterUsers);
        spinnerworks(view);
        filter="id";
        type="desc";
        searchoption = "";
        searchtext = "";
        pageNo = 1;
        requestForGetUsers("10", "1", myPrefs.getId(), filter, type, searchoption, searchtext);
        initScrollListener();

    }

    private void spinnerworks(View view) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1 && list.size() < totalRecords) {
                        pageNo = pageNo + 1;
                        loadMore("10", String.valueOf(pageNo), myPrefs.getId(),  filter, type, searchoption, searchtext);
                        isLoading = true;
                    }
                }
            }
        });

    }

    public void resetData(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        spin.setSelection(0);
        etSearch.setText("");
        filter=fltr;
        type=typ;
        searchoption = "";
        searchtext = "";
        pageNo = 1;
        requestForGetUsers("10", String.valueOf(pageNo), myPrefs.getId(),  filter, type, searchoption, searchtext);

        headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingId.setTag("ic_down");
        headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingUsername.setTag("ic_down");
        headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingEmail.setTag("ic_down");
        headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingMobile.setTag("ic_down");
        headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
        headingAmount.setTag("ic_down");
    }
    public void ascDesc(String fltr, String typ) {
        tvResponse.setVisibility(View.GONE);
        filter=fltr;
        type=typ;
        pageNo = 1;
        requestForGetUsers("10", String.valueOf(pageNo), myPrefs.getId(),  filter, type, searchoption, searchtext);
    }
    public void requestForGetUsers(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);
            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetUsers jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetUsers url : " + MyConstants.URL_CONTRACTOR_GETUSERS);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETUSERS, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetUsers response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetUsers responseGetUsers = gson.fromJson(response.toString(), ResponseGetUsers.class);
                                if (responseGetUsers.getError().equalsIgnoreCase("false")) {

                                    Logger.logDebug("kamal", " requestForGetUsers false : " + responseGetUsers.getError());
                                    if (!responseGetUsers.getContractors().getTotal_records().equals("0")) {

                                        totalRecords = Integer.parseInt(responseGetUsers.getContractors().getTotal_records());
                                        tvResponse.setVisibility(View.GONE);
                                        list.clear();
                                        list.addAll(responseGetUsers.getContractors().getContractors());
                                        adapterUsers.notifyDataSetChanged();
                                    } else {
                                        totalRecords = Integer.parseInt(responseGetUsers.getContractors().getTotal_records());
                                        list.clear();
                                        adapterUsers.notifyDataSetChanged();
                                        tvResponse.setVisibility(View.VISIBLE);
                                        tvResponse.setText("No Data Found");
                                    }
//
//                                    list.addAll(responseGetUsers.getContractors().getContractors());
//                                    adapterUsers.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(getActivity(), responseGetUsers.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetUsers error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETUSERS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void loadMore(String per_page_record, String page_no, String user_id, String filter, String type, String searchOption, String searchText) {
        list.add(null);
        adapterUsers.notifyItemInserted(list.size() - 1);
        try {
            PojoRequestGetUsersData pojoRequestGetUsersData = new PojoRequestGetUsersData();
            pojoRequestGetUsersData.setPer_page_record(per_page_record);
            pojoRequestGetUsersData.setPage_no(page_no);
            pojoRequestGetUsersData.setUser_id(user_id);
            pojoRequestGetUsersData.setFilter(filter);
            pojoRequestGetUsersData.setType(type);

            pojoRequestGetUsersData.setSearch_option(searchOption);
            pojoRequestGetUsersData.setSearch_text(searchText);
            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestGetUsersData);
            Logger.logDebug("kamal", " requestForGetUsers jsonString : " + jsonString);
            Logger.logDebug("kamal", " requestForGetUsers url : " + MyConstants.URL_CONTRACTOR_GETUSERS);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_GETUSERS, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Logger.logDebug("kamal", " requestForGetUsers response : " + response.toString());
                                Gson gson = new Gson();
                                ResponseGetUsers responseGetUsers = gson.fromJson(response.toString(), ResponseGetUsers.class);
                                if (responseGetUsers.getError().equalsIgnoreCase("false")) {

                                    Logger.logDebug("kamal", " requestForGetUsers false : " + responseGetUsers.getError());
                                    if (!responseGetUsers.getContractors().getTotal_records().equals("0")) {

                                        totalRecords = Integer.parseInt(responseGetUsers.getContractors().getTotal_records());
                                        list.remove(list.size() - 1);
                                        int scrollPosition = list.size();
                                        adapterUsers.notifyItemRemoved(scrollPosition);
                                        list.addAll(responseGetUsers.getContractors().getContractors());
                                        adapterUsers.notifyDataSetChanged();
                                        isLoading = false;

                                    } else {
                                    }

                                } else {
                                    Toast.makeText(getActivity(), responseGetUsers.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.logDebug("kamal", " requestForGetUsers error : " + error.getLocalizedMessage());
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_GETUSERS);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }

    }


    private void dialogOpeningApp() {
        dialogLoading = new Dialog(getActivity());
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading_singin);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.show();
        Window window = dialogLoading.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void dialogAddUser() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.dialog_adduser, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);
        String[] enaidisa = {"Enabled", "Disabled"};
        final ImageView ivCancel = (ImageView) promptsView.findViewById(R.id.imageview_cancel);
        final EditText etUsername = (EditText) promptsView.findViewById(R.id.edittext_username);
        final EditText etEmail = (EditText) promptsView.findViewById(R.id.edittext_email);
        final EditText etMobile = (EditText) promptsView.findViewById(R.id.edittext_mobile);
        final EditText etAmounttoAdd = (EditText) promptsView.findViewById(R.id.edittext_amounttoadd);
        final EditText etPassword = (EditText) promptsView.findViewById(R.id.edittext_password);
        final EditText etCommision = (EditText) promptsView.findViewById(R.id.edittext_commission);
        final EditText etCut = (EditText) promptsView.findViewById(R.id.edittext_cut);
        final TextView tvSubmit = (TextView) promptsView.findViewById(R.id.textview_submit);
        final Spinner spinerEnaDisa = (Spinner) promptsView.findViewById(R.id.spinner);
        ArrayAdapter<String> adapterEnaDisa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, enaidisa);
        adapterEnaDisa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerEnaDisa.setAdapter(adapterEnaDisa);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAddUser.dismiss();
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etUsername.getText().toString())) {
                    etUsername.setError("Please enter username");
                    Toast.makeText(getActivity(), "Please enter username", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    etEmail.setError("please enter email.");
                    Toast.makeText(getActivity(), "please enter email.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etMobile.getText().toString())) {
                    etMobile.setError("Please enter mobile");
                    Toast.makeText(getActivity(), "Please enter mobile", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etAmounttoAdd.getText().toString())) {
                    etAmounttoAdd.setError("Please enter amount");
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    etPassword.setError("Please enter password");
                    Toast.makeText(getActivity(), "Please enter password", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etCommision.getText().toString())) {
                    etCommision.setError("Please enter commission");
                    Toast.makeText(getActivity(), "Please enter commission", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etCut.getText().toString())) {
                    etCut.setError("Please enter cut");
                    Toast.makeText(getActivity(), "Please enter cut", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etCut.getText().toString())) {
                    etCut.setError("Please enter cut");
                    Toast.makeText(getActivity(), "Please enter cut", Toast.LENGTH_SHORT).show();
                } else {
                    requestForAddUser(myPrefs.getId(), etUsername.getText().toString().trim(), etPassword.getText().toString().trim(), etEmail.getText().toString().trim(), etMobile.getText().toString().trim(), etAmounttoAdd.getText().toString().trim(), spinerEnaDisa.getSelectedItem().toString().toLowerCase(), UsefullUtils.getLocalIpAddress(), "android", "android");
                }
            }
        });


        alertDialogBuilder.setCancelable(true);
        alertDialogAddUser = alertDialogBuilder.create();
        alertDialogAddUser.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        alertDialogAddUser.show();
    }

    private void requestForAddUser(String user_id, String username, String password, String email, String mobile, String amount, String account_status, String ip, String browser, String Os) {
        try {
            PojoRequestAddUser pojoRequestAddUser = new PojoRequestAddUser();
            pojoRequestAddUser.setUser_id(user_id);
            pojoRequestAddUser.setUsername(username);
            pojoRequestAddUser.setPassword(password);
            pojoRequestAddUser.setEmail(email);
            pojoRequestAddUser.setMobile(mobile);
            pojoRequestAddUser.setAmount(amount);
            pojoRequestAddUser.setAccount_status(account_status);
            pojoRequestAddUser.setIp(ip);
            pojoRequestAddUser.setBrowser(browser);
            pojoRequestAddUser.setOs(Os);

            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestAddUser);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_ADDUSER, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Gson gson = new Gson();
                                ResponseAddUser responseAddUser = gson.fromJson(response.toString(), ResponseAddUser.class);
                                if (responseAddUser.getError().equalsIgnoreCase("false")) {

                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_SHORT).show();
                                    resetData("id","desc");
                                    alertDialogAddUser.dismiss();

                                } else {
                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_ADDUSER);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUsersIdClickForTransactions(int position) {
//        Fragment someFragment = new SomeFragment();
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.replace(R.id.fragment_container, someFragment ); // give your fragment container id in first parameter
//        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
//        transaction.commit();
        TextView tvHeading = getActivity().findViewById(R.id.textview_heading);
        tvHeading.setText("Users Transaction");
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putString("searchoption", "user_id");
        bundle.putString("searchtext", list.get(position).getId());
        ContractorFragmentUserstransaction contractorFragmentUserstransaction = new ContractorFragmentUserstransaction();
        contractorFragmentUserstransaction.setArguments(bundle);

        ft.replace(R.id.container, contractorFragmentUserstransaction, "ContractorFragmentUserstransaction");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    @Override
    public void onUsersCreditDebitAdd(int position) {
        dialogUserCreditDebitAdd(position);
    }

    private void dialogUserCreditDebitAdd(final int position) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.dialog_creditdebitadd, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);


        final String[] strCreditDebit = {"Select", "Credit", "Debit"};
        final Spinner spinnerCreditDebit = (Spinner) promptsView.findViewById(R.id.spinner);
        ArrayAdapter<String> adapterCreditDebit = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, strCreditDebit);
        adapterCreditDebit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCreditDebit.setAdapter(adapterCreditDebit);


        final ImageView ivCancel = (ImageView) promptsView.findViewById(R.id.imageview_cancel);
        final EditText etAmount = (EditText) promptsView.findViewById(R.id.edittext_amount);
        final LinearLayout llOpt = (LinearLayout) promptsView.findViewById(R.id.linearlayout_otp);
        final EditText etOtp = (EditText) promptsView.findViewById(R.id.edittext_otp);
        final TextView tvOtp = (TextView) promptsView.findViewById(R.id.textview_getotp);
        final TextView tvSubmit = (TextView) promptsView.findViewById(R.id.textview_submit);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogUserCreditDebitAdd.dismiss();
            }
        });
        spinnerCreditDebit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                System.out.println("Heyganesha onItemSelected " + strCreditDebit[pos]);
                if (strCreditDebit[pos].equalsIgnoreCase("debit")) {
                    etOtp.setText("");
                    llOpt.setVisibility(View.VISIBLE);
                } else {
                    llOpt.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        tvOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestFordebitMoneyOtp(myPrefs.getId(), list.get(position).getId());
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strcreditOrDebit = spinnerCreditDebit.getSelectedItem().toString().toLowerCase().trim();
                String windowId = "0";
                if (strcreditOrDebit.equals("select")) {
                    Toast.makeText(getActivity(), "Please select credit or debit", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etAmount.getText().toString())) {
                    etAmount.setError("Please enter amount");
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    if (strcreditOrDebit.equals("credit")) {
                        requestForAddUserMoney(strcreditOrDebit, etAmount.getText().toString().trim(), list.get(position).getId(), myPrefs.getId(), "");
                    } else if (strcreditOrDebit.equals("debit")) {
                        if (TextUtils.isEmpty(etOtp.getText().toString())) {
                            etOtp.setError("Please enter otp");
                        } else {
                            requestForAddUserMoney(strcreditOrDebit, etAmount.getText().toString().trim(), list.get(position).getId(), myPrefs.getId(), etOtp.getText().toString().trim());
                        }
                    }
                }
            }
        });


        alertDialogBuilder.setCancelable(true);
        alertDialogUserCreditDebitAdd = alertDialogBuilder.create();
        alertDialogUserCreditDebitAdd.setCanceledOnTouchOutside(false);
        alertDialogUserCreditDebitAdd.show();
    }

    private void requestFordebitMoneyOtp(String user_id, String contractor_id) {
        try {
            PojoRequestDebitMoneyOtp pojoRequestDebitMoneyOtp = new PojoRequestDebitMoneyOtp();
            pojoRequestDebitMoneyOtp.setUser_id(user_id);
            pojoRequestDebitMoneyOtp.setContractor_id(contractor_id);


            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestDebitMoneyOtp);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_DEBITUSERMONEY, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Gson gson = new Gson();
                                ResponseAddUser responseAddUser = gson.fromJson(response.toString(), ResponseAddUser.class);
                                if (responseAddUser.getError().equalsIgnoreCase("false")) {

                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_LONG).show();

                                } else {
                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_DEBITMONEYOTP);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    private void requestForAddUserMoney(String money_type, String add_coin, String contractor_money_id, String user_id, String money_otp) {
        try {
            PojoRequestAddUserMoney pojoRequestAddUserMoney = new PojoRequestAddUserMoney();
            pojoRequestAddUserMoney.setMoney_type(money_type);
            pojoRequestAddUserMoney.setAdd_coin(add_coin);
            pojoRequestAddUserMoney.setContractor_money_id(contractor_money_id);
            pojoRequestAddUserMoney.setUser_id(user_id);
            pojoRequestAddUserMoney.setMoney_otp(money_otp);


            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestAddUserMoney);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_ADDUSERMONEY, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Gson gson = new Gson();
                                ResponseAddUser responseAddUser = gson.fromJson(response.toString(), ResponseAddUser.class);
                                if (responseAddUser.getError().equalsIgnoreCase("false")) {

                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_LONG).show();
                                    resetData("id","desc");
                                    alertDialogUserCreditDebitAdd.dismiss();

                                } else {
                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_ADDUSERMONEY);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUsersEdit(int position) {
        dialogEditUser(position, list.get(position).getUsername(), list.get(position).getEmail(), list.get(position).getMobile(), list.get(position).getAmount(), list.get(position).getAccount_status(), list.get(position).getCommission(), list.get(position).getCut());
    }

    private void dialogEditUser(final int pos, String username, String email, String mobile, String amount, String account_status, String commission, String cut) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.dialog_edituser, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);
        String[] enaidisa = {"Select", "Enabled", "Disabled"};
        final ImageView ivCancel = (ImageView) promptsView.findViewById(R.id.imageview_cancel);
        final TextView tvAmount = (TextView) promptsView.findViewById(R.id.textview_totalamount);
        final EditText etUsername = (EditText) promptsView.findViewById(R.id.edittext_username);
        final EditText etEmail = (EditText) promptsView.findViewById(R.id.edittext_email);
        final EditText etMobile = (EditText) promptsView.findViewById(R.id.edittext_mobile);
        final EditText etAmounttoAdd = (EditText) promptsView.findViewById(R.id.edittext_amounttoadd);
        final EditText etPassword = (EditText) promptsView.findViewById(R.id.edittext_password);
        final EditText etCommision = (EditText) promptsView.findViewById(R.id.edittext_commission);
        final EditText etCut = (EditText) promptsView.findViewById(R.id.edittext_cut);
        final TextView tvSubmit = (TextView) promptsView.findViewById(R.id.textview_submit);
        final Spinner spinerEnaDisa = (Spinner) promptsView.findViewById(R.id.spinner);
        ArrayAdapter<String> adapterEnaDisa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, enaidisa);
        adapterEnaDisa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerEnaDisa.setAdapter(adapterEnaDisa);
        if (account_status.equalsIgnoreCase("enabled")) {
            spinerEnaDisa.setSelection(1);
        } else if (account_status.equalsIgnoreCase("disabled")) {
            spinerEnaDisa.setSelection(2);
        } else {
            spinerEnaDisa.setSelection(0);
        }

        tvAmount.setText("Total Amount:- " + amount);
        etUsername.setText(username);
        etEmail.setText(email);
        etMobile.setText(mobile);
        etCommision.setText(commission);
        etCut.setText(cut);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogEditUser.dismiss();
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etUsername.getText().toString())) {
                    etUsername.setError("Please enter username");
                    Toast.makeText(getActivity(), "Please enter username", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    etEmail.setError("please enter email.");
                    Toast.makeText(getActivity(), "please enter email.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etMobile.getText().toString())) {
                    etMobile.setError("Please enter mobile");
                    Toast.makeText(getActivity(), "Please enter mobile", Toast.LENGTH_SHORT).show();
                }
//                else if (TextUtils.isEmpty(etAmounttoAdd.getText().toString())) {
//                    etAmounttoAdd.setError("Please enter amount");
//                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
//                }
                else if (spinerEnaDisa.getSelectedItem().toString().equals("Select")) {
                    Toast.makeText(getActivity(), "Please select enable or disable user", Toast.LENGTH_SHORT).show();
                } else {
                    requestForEditUser(myPrefs.getId(), etUsername.getText().toString().trim(), etPassword.getText().toString().trim(), etEmail.getText().toString().trim(), etMobile.getText().toString().trim(), etAmounttoAdd.getText().toString().trim(), spinerEnaDisa.getSelectedItem().toString().toLowerCase(), list.get(pos).getId(), "0", "android", "android");
                }
            }
        });


        alertDialogBuilder.setCancelable(true);
        alertDialogEditUser = alertDialogBuilder.create();
        alertDialogEditUser.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        alertDialogEditUser.show();
    }

    private void requestForEditUser(String user_id, String username, String password, String email, String mobile, String amount, String account_status, String contractorId, String ip, String browser, String Os) {
        try {
            PojoRequestAddUser pojoRequestAddUser = new PojoRequestAddUser();
            pojoRequestAddUser.setUser_id(user_id);
            pojoRequestAddUser.setUsername(username);
            pojoRequestAddUser.setPassword(password);
            pojoRequestAddUser.setEmail(email);
            pojoRequestAddUser.setMobile(mobile);
            pojoRequestAddUser.setAmount(amount);
            pojoRequestAddUser.setAccount_status(account_status);
            pojoRequestAddUser.setContractor_id(contractorId);
//            pojoRequestAddUser.setIp(ip);
//            pojoRequestAddUser.setBrowser(browser);
//            pojoRequestAddUser.setOs(Os);

            Gson gson = new Gson();
            String jsonString = gson.toJson(pojoRequestAddUser);
            JSONObject request = new JSONObject(jsonString);
            if (request != null) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MyConstants.URL_CONTRACTOR_EDITUSER, request, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        try {
                            if (response != null) {
                                Gson gson = new Gson();
                                ResponseAddUser responseAddUser = gson.fromJson(response.toString(), ResponseAddUser.class);
                                if (responseAddUser.getError().equalsIgnoreCase("false")) {

                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_SHORT).show();
                                    resetData("id","desc");
                                    alertDialogEditUser.dismiss();

                                } else {
                                    Toast.makeText(getActivity(), responseAddUser.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (dialogLoading.isShowing()) {
                            dialogLoading.dismiss();
                        }
                        Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Api-Key", "2837e11790d4d5cec1c9cab60c7d685b");
                        return headers;
                    }

                };

                volleyWrappers.addToRequestQueue(jsonObjectRequest, TAG_EDITUSER);
                dialogOpeningApp();
            }

        } catch (Exception e) {
            Logger.logDebug("kamal", "Exception e : " + e.getLocalizedMessage());
            Toast.makeText(getActivity(), MES_OOPS, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if(view==headingId){
            if(headingId.getTag().equals("ic_down")){
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingId.setTag("ic_up");
                ascDesc("id","asc");
            }else{
                headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingId.setTag("ic_down");
                ascDesc("id","desc");
            }
            headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUsername.setTag("ic_down");
            headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingEmail.setTag("ic_down");
            headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingMobile.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");

        }
        if(view==headingUsername){
            if(headingUsername.getTag().equals("ic_down")){
                headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingUsername.setTag("ic_up");
                ascDesc("username","asc");
            }else{
                headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingUsername.setTag("ic_down");
                ascDesc("username","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingEmail.setTag("ic_down");
            headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingMobile.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
        }
        if(view==headingEmail){
            if(headingEmail.getTag().equals("ic_down")){
                headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingEmail.setTag("ic_up");
                ascDesc("email","asc");
            }else{
                headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingEmail.setTag("ic_down");
                ascDesc("email","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUsername.setTag("ic_down");
            headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingMobile.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");

        }
        if(view==headingMobile){
            if(headingMobile.getTag().equals("ic_down")){
                headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingMobile.setTag("ic_up");
                ascDesc("mobile","asc");
            }else{
                headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingMobile.setTag("ic_down");
                ascDesc("mobile","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUsername.setTag("ic_down");
            headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingEmail.setTag("ic_down");
            headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingAmount.setTag("ic_down");
        }
        if(view==headingAmount){
            if(headingAmount.getTag().equals("ic_down")){
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_up, 0);
                headingAmount.setTag("ic_up");
                ascDesc("amount","asc");
            }else{
                headingAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
                headingAmount.setTag("ic_down");
                ascDesc("amount","desc");
            }
            headingId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingId.setTag("ic_down");
            headingUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingUsername.setTag("ic_down");
            headingEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingEmail.setTag("ic_down");
            headingMobile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_down, 0);
            headingMobile.setTag("ic_down");
        }


    }
}