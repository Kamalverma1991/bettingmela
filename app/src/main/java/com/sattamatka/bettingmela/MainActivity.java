package com.sattamatka.bettingmela;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.sattamatka.bettingmela.activity.ContractorRegistration;
import com.sattamatka.bettingmela.fragment.ContractorFragmentDashboard;
import com.sattamatka.bettingmela.fragment.ContractorFragmentSheets;
import com.sattamatka.bettingmela.fragment.ContractorFragmentUserprofile;
import com.sattamatka.bettingmela.fragment.ContractorFragmentUsers;
import com.sattamatka.bettingmela.fragment.ContractorFragmentUserstransaction;
import com.sattamatka.bettingmela.utils.MyPrefs;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private boolean doubleBackToExit = false;
    private TextView tvHeading;
    private TextView tvReset;
    Handler handlerSmooth;
    private MyPrefs myPrefs;

//    private TextView tvHead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolBar();
        setDrawer();
        initViews();
        if (savedInstanceState == null) {
            loadfragment(0);
        }
    }

    private void setToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        tvHead = (TextView) toolbar.findViewById(R.id.toolbar_title);
//        tvHead.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/customsursive.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setDrawer() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }


            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//        toggle.setDrawerIndicatorEnabled(false);
//        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_drawer, getTheme());
//        toggle.setHomeAsUpIndicator(drawable);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu_drawer);


    }

    private void initViews() {
        myPrefs = new MyPrefs(MainActivity.this);
        handlerSmooth = new Handler();
        tvHeading = (TextView) findViewById(R.id.textview_heading);
        tvReset = (TextView) findViewById(R.id.text_balance_toolbar);
        tvReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==tvReset){
            ContractorFragmentSheets contractorFragmentSheets = (ContractorFragmentSheets) getSupportFragmentManager().findFragmentByTag("ContractorFragmentSheets");
            ContractorFragmentUsers contractorFragmentUsers = (ContractorFragmentUsers) getSupportFragmentManager().findFragmentByTag("ContractorFragmentUsers");
            ContractorFragmentUserstransaction contractorFragmentUserstransaction = (ContractorFragmentUserstransaction) getSupportFragmentManager().findFragmentByTag("ContractorFragmentUserstransaction");

            if (contractorFragmentSheets != null && contractorFragmentSheets.isVisible()) {
                contractorFragmentSheets.resetData("id","desc");
            }
            if (contractorFragmentUsers != null && contractorFragmentUsers.isVisible()) {
                contractorFragmentUsers.resetData("id","desc");
            }
            if (contractorFragmentUserstransaction != null && contractorFragmentUserstransaction.isVisible()) {
                contractorFragmentUserstransaction.resetData("id","desc");
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            ContractorFragmentDashboard contractorFragmentDashboard = (ContractorFragmentDashboard) getSupportFragmentManager().findFragmentByTag("ContractorFragmentDashboard");
             if (contractorFragmentDashboard != null && contractorFragmentDashboard.isVisible()) {
                if (doubleBackToExit) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExit = true;
                Toast.makeText(this, getString(R.string.click_back), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExit = false;
                    }
                }, 2000);

            } else {
                loadfragment(0);
            }
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navcontractor_dashboard) {
            smoothness(0);
        } else if (id == R.id.navcontractor_userprofile) {
            smoothness(1);
        } else if (id == R.id.navcontractor_sheets) {
            smoothness(2);
        } else if (id == R.id.navcontractor_users) {
            smoothness(3);
        } else if (id == R.id.navcontractor_userstransaction) {
            smoothness(4);
        } else if (id == R.id.navcontractor_logout) {
            myPrefs.setLogincontractor(false);
            myPrefs.setId("");
            myPrefs.setUsername("");
            myPrefs.setMobile("");
            myPrefs.setAmount("");
            startActivity(new Intent(MainActivity.this, ContractorRegistration.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void smoothness(final int position) {

        drawer.closeDrawer(GravityCompat.START);
        handlerSmooth.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadfragment(position);
            }
        }, 250);
    }

    public  void loadfragment(int open) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (open == 0) {
            tvHeading.setText("Dasboard");
            tvReset.setVisibility(View.GONE);
            ft.replace(R.id.container, new ContractorFragmentDashboard(), "ContractorFragmentDashboard");
        } else if (open == 1) {
            tvReset.setVisibility(View.GONE);
            tvHeading.setText("Profile");
            ft.replace(R.id.container, new ContractorFragmentUserprofile(), "ContractorFragmentUserprofile");
        } else if (open == 2) {
            tvReset.setVisibility(View.VISIBLE);
            tvHeading.setText("Sheets");
            ft.replace(R.id.container, new ContractorFragmentSheets(), "ContractorFragmentSheets");
        } else if (open == 3) {
            tvReset.setVisibility(View.VISIBLE);
            tvHeading.setText("Users");
            ft.replace(R.id.container, new ContractorFragmentUsers(), "ContractorFragmentUsers");
        } else if (open == 4) {
            tvReset.setVisibility(View.VISIBLE);
            tvHeading.setText("Users Transaction");
            ft.replace(R.id.container, new ContractorFragmentUserstransaction(), "ContractorFragmentUserstransaction");
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }


}
