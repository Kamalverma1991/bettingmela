package com.sattamatka.bettingmela;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ExecutionException;


/**
 * Project           : VitaSkin
 * File Name         : VolleyWrappers
 * Description       : Wrapper to do volley requests
 * Revision History: version 1:
 * Date: 2/16/17
 * Original author: pradeep
 * Description: Initial version
 */
public class VolleyWrappers {
    public static final String TAG = VolleyWrappers.class.getSimpleName();
    private static final int TIME_OUT = 5000;
    private static final int MAX_RETRIES = 3;
    private RequestQueue requestQueue = null;
    private Context context = null;

    public VolleyWrappers(Context context) {
        this.context = context;
        if (requestQueue == null) {
            //requestQueue = Volley.newRequestQueue(context,
            //        new OkHttpStack(getConfiguredHttpsClient()));
            requestQueue = Volley.newRequestQueue(context);
        }
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        cancelPendingRequests(tag);
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(getDefaultRetryPolicy());
        requestQueue.add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        cancelPendingRequests(TAG);
        req.setTag(TAG);
        req.setRetryPolicy(getDefaultRetryPolicy());
        requestQueue.add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public JSONObject synchronousRequest(String url, String tag, Map<String, String> map1) throws InterruptedException, ExecutionException {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(map1), future, future);
        addToRequestQueue(request, tag);
        JSONObject response = future.get(); // blocking call
        return response;
    }

    public JSONObject synchronousRequest(String url, String tag) throws InterruptedException, ExecutionException {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(), future, future);
        addToRequestQueue(request, tag);
        JSONObject response = future.get(); // blocking call
        return response;
    }


    public JSONObject synchronousRequestArray(String url, String tag) throws InterruptedException, ExecutionException, JSONException {
        RequestFuture<JSONArray> future = RequestFuture.newFuture();
        JsonArrayRequest request = new JsonArrayRequest(url,
                future, future);
        addToRequestQueue(request, tag);
        JSONArray response = future.get(); // blocking call
        return response.getJSONObject(0);
    }

    private DefaultRetryPolicy getDefaultRetryPolicy() {
        return new DefaultRetryPolicy(TIME_OUT,
                MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

}
